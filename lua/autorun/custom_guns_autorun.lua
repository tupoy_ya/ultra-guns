CHAN_UG_CHANNEL = 20

UGBase = UGBase or {}
UGBase.cached_particles = UGBase.cached_particles or {}
UGBase.cached_cvars = UGBase.cached_cvars or {}
UGBase.cvars_desc = UGBase.cvars_desc or {}
UGBase.ammo_types = UGBase.ammo_types or {}
UGBase.max_ricochets = UGBase.max_ricochets or {}
UGBase.max_penetrate = UGBase.max_penetrate or {}

function UGBase.Msg(...)
	Msg("[UGBase] ") print(...)
end

function UGBase.AddParticle(name, name_2)
	if UGBase.cached_particles[name] and UGBase.cached_particles[name][name_2] then
		return true
	end

	if not name or not name_2 then
		return false
	end

	game.AddParticles(name)
	PrecacheParticleSystem(name_2)

	UGBase.cached_particles[name] = UGBase.cached_particles[name] or {}
	UGBase.cached_particles[name][name_2] = true

	return true
end

function UGBase.AddSoundFire(name, soundname, volume, pitch, level)
	assert(name)
	assert(soundname)

	return sound.Add({
		name = name,
		volume = volume or 1,
		pitch = pitch or {95, 105},
		sound = soundname,
		channel = CHAN_STATIC,
		level = 80
	})
end

function UGBase.CVARCreate(cvar, default_value, description, category, type, ...)
	local vars, founded = {...}, false

	UGBase.cvars_desc[category] = UGBase.cvars_desc[category] or {}

	for _, t in next, UGBase.cvars_desc[category] do
		if t[1] == cvar then
			founded = true

			break
		end
	end

	if not founded then
		table.insert(UGBase.cvars_desc[category], {cvar, description, type, vars[1], vars[2], ["default_value"] = default_value, ["isCLIENT"] = true})
	end

	return CreateConVar(cvar, default_value, FCVAR_ARCHIVE, description, vars[1] or nil, vars[2] or nil)
end

local cvar_flags = SERVER and bit.bor(FCVAR_ARCHIVE, FCVAR_REPLICATED, FCVAR_NOTIFY) or FCVAR_REPLICATED

function UGBase.ServerCVARCreate(cvar, default_value, description, category)
	local founded = false

	UGBase.cvars_desc[category] = UGBase.cvars_desc[category] or {}

	for _, t in next, UGBase.cvars_desc[category] do
		if t[1] == cvar then
			founded = true

			break
		end
	end

	if not founded then
		table.insert(UGBase.cvars_desc[category], {cvar, description, ["default_value"] = default_value, ["isCLIENT"] = false})
	end

	return CreateConVar(cvar, default_value, cvar_flags, description)
end

function UGBase.CVARChangeCallback(cvar, func)
	assert(cvar)
	assert(func)

	if UGBase.cached_cvars[cvar] then
		return false
	end

	UGBase.cached_cvars[cvar] = true

	return cvars.AddChangeCallback(cvar, function(_, _, val) timer.Simple(FrameTime(), function() func(val) end) end)
end

function UGBase.AddSound(name, soundname, volume, pitch)
	assert(name)
	assert(soundname)

	return sound.Add({
		name = name,
		channel = CHAN_UG_CHANNEL,
		volume = volume or 1,
		pitch = pitch or {95, 105},
		sound = soundname
	})
end

function UGBase.AddAmmoType(name, maxricochets, maxpenetrate, ent_give, ent_model)
	assert(name)

	game.AddAmmoType({name = name, dmgtype = DMG_BULLET, tracer = TRACER_LINE_AND_WHIZ, maxsplash = 6, minsplash = 4})

	UGBase.ammo_types[name] = {ent_give, ent_model}
	UGBase.max_ricochets[name] = maxricochets or 1
	UGBase.max_penetrate[name] = maxpenetrate or 10

	if CLIENT then
		language.Add(name .. "_ammo", name)
	end
end

local vec_250 = Vector(0, 0, 250)
local angle_zero, vec_15 = Angle(), Vector(0, 0, 16.5)

function UGBase.RegisterAmmoBoxEnt(ammo, ammo_type, model)
	local ENT = {}

	ENT.PrintName = ammo
	ENT.Category = "Ultra Guns"
	ENT.Base = "base_gmodentity"
	ENT.Type = "anim"
	ENT.AdminOnly = false
	ENT.Spawnable = true
	ENT.HP = 100

	function ENT:SetupDataTables()
		self:NetworkVar("Float", 0, "NextUse")
		self:NetworkVar("Float", 1, "Limit")
	end

	if SERVER then
		function ENT:Initialize()
			self:SetModel(model)
			self:PhysicsInit(6)
			self:SetMoveType(6)
			self:SetSolid(6)
			self:PhysWake()
			self:SetLimit(8)
		end

		function ENT:Use(ent)
			if self:GetNextUse() > CurTime() then
				return
			end

			if ent:IsPlayer() then
				if ammo_type == "universal" then
					local wep = ent:GetActiveWeapon()

					if wep:IsValid() and wep.IsUGBase then
						ent:GiveAmmo(wep:GetMaxClip1(), wep:GetPrimaryAmmoType())

						self:SetNextUse(CurTime() + 0.5)
						self:SetLimit(self:GetLimit() - 1)
					end
				else
					ent:GiveAmmo(ammo_type, ammo)

					self:SetNextUse(CurTime() + 0.5)
					self:SetLimit(self:GetLimit() - 1)
				end
			end

			if self:GetLimit() <= 0 then
				self:Remove()
			end
		end

		function ENT:OnTakeDamage(dmg)
			if self.HP <= 0 then
				return
			end

			self.HP = self.HP - dmg:GetDamage()

			if self.HP <= 0 and not self.Killed then
				local phys = self:GetPhysicsObject()

				if phys:IsValid() then
					phys:SetVelocity(vec_250)
					phys:AddAngleVelocity(VectorRand() * 180)
				end

				self.Killed = true

				timer.Simple(.28, function()
					if not self:IsValid() then
						return
					end

					local explode = ents.Create("env_explosion")
					explode:SetPos(self:GetPos())
					explode:Spawn()
					explode:SetKeyValue("iMagnitude", "80")
					explode:Fire("Explode", 0, 0)

					self:Remove()
				end)
			end
		end
	else
		local color_white, color_black = color_white, color_black

		function ENT:Draw()
			self:DrawModel()

			if EyePos():DistToSqr(self:GetPos()) > 800000 then
				return
			end

			local txt = string.format("%s\n%i/8", self.PrintName, self:GetLimit())
			local _, h = surface.GetTextSize(txt)
			local ang = angle_zero + EyeAngles()

			ang:RotateAroundAxis(ang:Right(), 90)
			ang:RotateAroundAxis(ang:Up(), -90)

			local vec = self:LocalToWorld(vec_15)

			if not self.Length then
				self.Length = (self:OBBMaxs() - self:OBBMins()):Length()
			end

			if self.Length > 30 then
				vec:Add(Vector(0, 0, 20))
			end

			cam.Start3D2D(vec, ang, 0.04)
				draw.DrawText(txt, "ug_SENTFont", 3, 3 - h / 2, color_black, 1)
				draw.DrawText(txt, "ug_SENTFont", 0, 0 - h / 2, color_white, 1)
			cam.End3D2D()
		end
	end

	scripted_ents.Register(ENT, "ug_" .. string.Replace(string.lower(ammo), " ", ""))
end

hook.Add("Initialize", "ug_RegisterAmmo", function()
	hook.Remove("Initialize", "ug_RegisterAmmo")

	UGBase.AddAmmoType(".45 ACP", 1, 8, 90, "models/Items/BoxSRounds.mdl")
	UGBase.AddAmmoType(".50 Magnum", 1, 10, 21, "models/Items/BoxSRounds.mdl")
	UGBase.AddAmmoType(".357 Magnum", 1, 10, 21, "models/Items/BoxSRounds.mdl")
	UGBase.AddAmmoType("12 Gauge", 1, 12, 24, "models/Items/boxbuckshot.mdl")
	UGBase.AddAmmoType("5.56x39MM", 2, 8, 90, "models/Items/BoxMRounds.mdl")
	UGBase.AddAmmoType("5.56x45MM", 2, 8, 90, "models/Items/BoxMRounds.mdl")
	UGBase.AddAmmoType("7.62x33MM", 2, 9, 90, "models/Items/BoxMRounds.mdl")
	UGBase.AddAmmoType("7.62x39MM", 2, 9, 90, "models/Items/BoxMRounds.mdl")
	UGBase.AddAmmoType("7.62x51MM", 2, 10, 90, "models/Items/BoxMRounds.mdl")

	for ammo, t in pairs(UGBase.ammo_types) do
		UGBase.RegisterAmmoBoxEnt(ammo, t[1], t[2])
	end

	UGBase.RegisterAmmoBoxEnt("Universal Ammo Box", "universal", "models/Items/item_item_crate.mdl")
end)





UGBase.AddParticle("particles/cso2_smoke.pcf", "smoke_rifles")
UGBase.AddParticle("particles/muzzleflashes.pcf", "muzzle_pistol")

-- [[ Sounds ]] --

UGBase.AddSound("Universal.Draw", {"weapons/thanez357/uni_pistol_draw_01.wav", "weapons/thanez357/uni_pistol_draw_02.wav", "weapons/thanez357/uni_pistol_draw_03.wav"})
UGBase.AddSound("Universal.Cloth", "weapons/cloth.wav")
UGBase.AddSound("Universal.PistolDraw", "weapons/c96/uni_pistol_draw_01.wav")
UGBase.AddSound("Universal.LeanIn", {"weapons/thanez357/uni_lean_in_01.wav", "weapons/thanez357/uni_lean_in_02.wav", "weapons/thanez357/uni_lean_in_03.wav", "weapons/thanez357/uni_lean_in_04.wav"})
UGBase.AddSound("Universal.LeanOut", {"weapons/thanez357/uni_lean_out_01.wav", "weapons/thanez357/uni_lean_out_02.wav", "weapons/thanez357/uni_lean_out_03.wav", "weapons/thanez357/uni_lean_out_04.wav"})

-- Desert Eagle
	UGBase.AddSoundFire("DE.Fire", "weapons/desert_eagle_ug/deagle-1.wav", 1.25)
	UGBase.AddSound("DE.Deploy", "weapons/desert_eagle_ug/de_deploy.wav")
	UGBase.AddSound("DE.Clipin", "weapons/desert_eagle_ug/de_clipin.wav")
	UGBase.AddSound("DE.Clipout", "weapons/desert_eagle_ug/de_clipout.wav")
	UGBase.AddSound("DE.Slide", "weapons/desert_eagle_ug/de_slide.wav")
	UGBase.AddSound("DE.Back", "weapons/desert_eagle_ug/de_back.wav")

-- Mauser C96
	UGBase.AddSoundFire("Weapon_C96.Fire", "weapons/c96/c96_fp.wav")
	UGBase.AddSound("Weapon_C96.Boltback", "weapons/c96/c96_boltback.wav")
	UGBase.AddSound("Weapon_C96.Boltrelease", "weapons/c96/c96_boltrelease.wav")
	UGBase.AddSound("Weapon_C96.MagFetch", "weapons/c96/c96_magfetch.wav")
	UGBase.AddSound("Weapon_C96.ClipIn", "weapons/c96/c96_strippr_clip_in.wav")
	UGBase.AddSound("Weapon_C96.RoundsIn_01", "weapons/c96/c96_strippr_clip_rounds_in_01.wav")
	UGBase.AddSound("Weapon_C96.RoundsIn_02", "weapons/c96/c96_strippr_clip_rounds_in_02.wav")
	UGBase.AddSound("Weapon_C96.ClipRemove", "weapons/c96/c96_strippr_clip_remove.wav")
	UGBase.AddSound("Weapon_C96.Rattle", "weapons/c96/c96_rattle.wav")
	UGBase.AddSound("Weapon_C96.MagFiddle", "weapons/c96/c96_magfiddle.wav")

-- Colt
	UGBase.AddSoundFire("Weapon_revolver.Fire", "weapons/thanez357/revolver_fire.wav", nil, {85, 95})
	UGBase.AddSound("Weapon_revolver.OpenChamber", "weapons/thanez357/revolver_open_chamber.wav")
	UGBase.AddSound("Weapon_revolver.CloseChamber", "weapons/thanez357/revolver_close_chamber.wav")
	UGBase.AddSound("Weapon_revolver.CockHammer", {"weapons/thanez357/revolver_cock_hammer.wav", "weapons/thanez357/revolver_cock_hammer_ready.wav"})
	UGBase.AddSound("Weapon_revolver.DumpRounds", {"weapons/thanez357/revolver_dump_rounds_01.wav", "weapons/thanez357/revolver_dump_rounds_02.wav", "weapons/thanez357/revolver_dump_rounds_03.wav"})
	UGBase.AddSound("Weapon_revolver.RoundInsertSingle1", "weapons/thanez357/revolver_round_insert_single_01.wav")
	UGBase.AddSound("Weapon_revolver.RoundInsertSingle2", "weapons/thanez357/revolver_round_insert_single_02.wav")
	UGBase.AddSound("Weapon_revolver.RoundInsertSingle3", "weapons/thanez357/revolver_round_insert_single_03.wav")
	UGBase.AddSound("Weapon_revolver.RoundInsertSingle4", "weapons/thanez357/revolver_round_insert_single_04.wav")
	UGBase.AddSound("Weapon_revolver.RoundInsertSingle5", "weapons/thanez357/revolver_round_insert_single_05.wav")
	UGBase.AddSound("Weapon_revolver.RoundInsertSingle6", "weapons/thanez357/revolver_round_insert_single_06.wav")
	UGBase.AddSound("Weapon_revolver.RoundInsertSpeedLoader", "weapons/thanez357/revolver_speed_loader_insert.wav")

-- Double Barrel
	UGBase.AddSoundFire("dbarrel.fire", "weapons/dbarrel_ug/deagle-1.wav", nil, {70, 80})
	UGBase.AddSound("dbarrel.close", "weapons/dbarrel_ug/close.wav")
	UGBase.AddSound("dbarrel.cloth", "weapons/dbarrel_ug/cloth.wav")
	UGBase.AddSound("dbarrel.cloth2", "weapons/dbarrel_ug/cloth2.wav")
	UGBase.AddSound("dbarrel.insert1", "weapons/dbarrel_ug/insert1.wav")
	UGBase.AddSound("dbarrel.insert2", "weapons/dbarrel_ug/insert2.wav")
	UGBase.AddSound("dbarrel.magrelease", "weapons/dbarrel_ug/magrelease.wav")
	UGBase.AddSound("dbarrel.open", "weapons/dbarrel_ug/open.wav")
	UGBase.AddSound("dbarrel.rattle", "weapons/dbarrel_ug/rattle.wav")

-- M3 / W1200
	UGBase.AddSoundFire("m3.fire", "weapons/KimM3/m3_shoot_pump.wav")
	UGBase.AddSound("M3.Insert", "weapons/KimM3/insert.wav")
	UGBase.AddSound("M3.Handle", "weapons/KimM3/handle.wav")
	UGBase.AddSound("M3.Draw", "weapons/KimM3/draw.wav")
	UGBase.AddSound("M3.Foley", "weapons/KimM3/draw.wav", nil, {50, 75})
	UGBase.AddSound("M3.StockClick", "weapons/KimM3/click.wav")
	UGBase.AddSound("M3.Pump", "weapons/KimM3/pump.wav")

-- ACR
	UGBase.AddSoundFire("Masada.Single", "weapons/masadamagpul/masada_unsil.wav")
	UGBase.AddSound("Masada.Cloth1", "weapons/masadamagpul/cloth1.wav")
	UGBase.AddSound("Masada.Cloth2", "weapons/masadamagpul/cloth2.wav")
	UGBase.AddSound("Masada.Magin1", "weapons/masadamagpul/magin1.wav")
	UGBase.AddSound("Masada.Magin2", "weapons/masadamagpul/magin2.wav")
	UGBase.AddSound("Masada.Foley", "weapons/masadamagpul/foley.wav")
	UGBase.AddSound("Masada.Magout", "weapons/masadamagpul/magout.wav")
	UGBase.AddSound("Masada.Magslap", "weapons/masadamagpul/magslap.wav")
	UGBase.AddSound("Masada.Safety", "weapons/masadamagpul/safety.wav")
	UGBase.AddSound("Masada.Chargerback", "weapons/masadamagpul/chargerback.wav")
	UGBase.AddSound("Masada.Boltrelease", "weapons/masadamagpul/boltrelease.wav")
	UGBase.AddSound("Masada.Placesilencer", "weapons/masadamagpul/placesilencer.wav")
	UGBase.AddSound("Masada.Removesilencer", "weapons/masadamagpul/removesilencer.wav")

-- Famas
	UGBase.AddSoundFire("FC2_Famas.Single", "weapons/Famas_1/Famas-1.wav")
	UGBase.AddSound("FC2_Famas.Foley", "weapons/Famas_1/Foley.wav")
	UGBase.AddSound("FC2_Famas.Forearm", "weapons/Famas_1/Forearm.wav")
	UGBase.AddSound("FC2_Famas.Hit", "weapons/Famas_1/Hit.wav")
	UGBase.AddSound("FC2_Famas.Magfix", "weapons/Famas_1/Magfix.wav")
	UGBase.AddSound("FC2_Famas.Magout", "weapons/Famas_1/Magout.wav")
	UGBase.AddSound("FC2_Famas.Magrelease", "weapons/Famas_1/Magrelease.wav")
	UGBase.AddSound("FC2_Famas.Rustle", "weapons/Famas_1/Rustle.wav")
	UGBase.AddSound("FC2_Famas.Magin", "weapons/Famas_1/Magin.wav")

-- Thompson M1928a1
	UGBase.AddSoundFire("Weapon_m1928a1.Single", "weapons/m1928a1/fire.wav")
	UGBase.AddSound("Weapon_m1928a1.Draw", "weapons/m1928a1/draw.wav")
	UGBase.AddSound("Weapon_m1928a1.Boltrelease", "weapons/m1928a1/boltrelease.wav")
	UGBase.AddSound("Weapon_m1928a1.Magout", "weapons/m1928a1/magout.wav")
	UGBase.AddSound("Weapon_m1928a1.MagoutRattle", "weapons/m1928a1/magout_rattle.wav")
	UGBase.AddSound("Weapon_m1928a1.Magin", "weapons/m1928a1/magin.wav")
	UGBase.AddSound("Weapon_m1928a1.Hit", "weapons/m1928a1/maghit.wav")
	UGBase.AddSound("Weapon_m1928a1.Rattle", "weapons/m1928a1/rattle.wav")
	UGBase.AddSound("Weapon_m1928a1.Magtap", "weapons/m1928a1/magtap.wav")
	UGBase.AddSound("Weapon_m1928a1.Magrelease", "weapons/m1928a1/magrelease.wav")

-- R700
	UGBase.AddSoundFire("Weapon_R700.Single", {"weapons/R700/R700-1.wav", "weapons/R700/R700-2.wav", "weapons/R700/R700-3.wav", "weapons/R700/R700-4.wav", "weapons/R700/R700-5.wav"})
	UGBase.AddSound("Weapon_R700.Bolt", "weapons/R700/R700_bolt.wav")
	UGBase.AddSound("Weapon_R700.BoltUp", "weapons/R700/R700_bolt_up.wav")
	UGBase.AddSound("Weapon_R700.BoltBack", "weapons/R700/R700_bolt_back.wav")
	UGBase.AddSound("Weapon_R700.BoltForward", "weapons/R700/R700_bolt_forward.wav")
	UGBase.AddSound("Weapon_R700.MagRelease", "weapons/R700/R700_mag_release.wav")
	UGBase.AddSound("Weapon_R700.MagGrab", "weapons/R700/R700_mag_grab.wav")
	UGBase.AddSound("Weapon_R700.MagOut", "weapons/R700/R700_mag_out.wav")
	UGBase.AddSound("Weapon_R700.MagThrow", "weapons/R700/R700_mag_throw.wav")
	UGBase.AddSound("Weapon_R700.MagTake", "weapons/R700/R700_mag_throw.wav", 0.001)
	UGBase.AddSound("Weapon_R700.MagContact", "weapons/R700/R700_mag_contact.wav")
	UGBase.AddSound("Weapon_R700.MagIn", "weapons/R700/R700_mag_in.wav")
	UGBase.AddSound("Weapon_R700.Grab_1", "weapons/R700/R700_grab_1.wav")
	UGBase.AddSound("Weapon_R700.Grab_2", "weapons/R700/R700_grab_2.wav")
	UGBase.AddSound("Weapon_R700.Rattle", "weapons/R700/R700_Rattle.wav")

-- FN FAL
	UGBase.AddSoundFire("Weapon_fnfal2.Single", "weapons/fnfal_ug/galil-1.wav")
	UGBase.AddSound("Weapon_fnfal2.draw", "weapons/fnfal_ug/draw.wav")
	UGBase.AddSound("Weapon_fnfal2.holster", "weapons/fnfal_ug/holster.wav")
	UGBase.AddSound("Weapon_fnfal2.magout", "weapons/fnfal_ug/magout.wav")
	UGBase.AddSound("Weapon_fnfal2.cloth", "weapons/fnfal_ug/cloth.wav")
	UGBase.AddSound("Weapon_fnfal2.maginsert", "weapons/fnfal_ug/maginsert.wav")
	UGBase.AddSound("Weapon_fnfal2.magin", "weapons/fnfal_ug/magin.wav")
	UGBase.AddSound("Weapon_fnfal2.magtap", "weapons/fnfal_ug/magtap.wav")
	UGBase.AddSound("Weapon_fnfal2.boltback", "weapons/fnfal_ug/boltback.wav")
	UGBase.AddSound("Weapon_fnfal2.boltrelease", "weapons/fnfal_ug/boltrelease.wav")

-- M1 Carbine
	UGBase.AddSoundFire("Weapon_m1carbine.Single", "weapons/m1carbine/fire.wav")
	UGBase.AddSound("Weapon_m1carbine.Magrelease", "weapons/m1carbine/magrelease.wav")
	UGBase.AddSound("Weapon_m1carbine.Magout", "weapons/m1carbine/magout.wav")
	UGBase.AddSound("Weapon_m1carbine.Magin", "weapons/m1carbine/magin.wav")
	UGBase.AddSound("Weapon_m1carbine.Draw", "weapons/m1carbine/draw.wav")
	UGBase.AddSound("Weapon_m1carbine.Boltback", "weapons/m1carbine/boltback.wav")
	UGBase.AddSound("Weapon_m1carbine.Boltrelease", "weapons/m1carbine/boltrelease.wav")

-- L85
	UGBase.AddSoundFire("L85.Fire", "weapons/L85/Famas-1.wav")
	UGBase.AddSound("L85.Deploy", "weapons/L85/Deploy.wav", 0.001)
	UGBase.AddSound("L85.Boltpull", "weapons/L85/Boltpull.wav")
	UGBase.AddSound("L85.Boltrelease", "weapons/L85/Boltrelease.wav")
	UGBase.AddSound("L85.Cloth1", "weapons/L85/Cloth1.wav")
	UGBase.AddSound("L85.Cloth2", "weapons/L85/Cloth2.wav")
	UGBase.AddSound("L85.Foley", "weapons/L85/Foley.wav")
	UGBase.AddSound("L85.Magin", "weapons/L85/Magin.wav")
	UGBase.AddSound("L85.Magout", "weapons/L85/Magout.wav")
	UGBase.AddSound("L85.Magrelease", "weapons/L85/Magrelease.wav")
	UGBase.AddSound("L85.Rattle", "weapons/L85/Rattle.wav")

-- Weapon_AK_Alpha
	UGBase.AddSoundFire("Weapon_AK_Alpha.Fire", "weapons/insurgency_s_ak_alpha/ak74-1.mp3")
	UGBase.AddSound("Weapon_AK_Alpha.Boltback", "weapons/insurgency_s_ak_alpha/ak74_boltback.wav")
	UGBase.AddSound("Weapon_AK_Alpha.Boltrelease", "weapons/insurgency_s_ak_alpha/ak74_boltrelease.wav")
	UGBase.AddSound("Weapon_AK_Alpha.MagRelease", "weapons/insurgency_s_ak_alpha/ak74_magrelease.wav")
	UGBase.AddSound("Weapon_AK_Alpha.Magout", "weapons/insurgency_s_ak_alpha/ak74_magout.wav")
	UGBase.AddSound("Weapon_AK_Alpha.Rattle", "weapons/insurgency_s_ak_alpha/ak74_rattle.wav")
	UGBase.AddSound("Weapon_AK_Alpha.MagoutRattle", "weapons/insurgency_s_ak_alpha/ak74_magoutrattle.wav")
	UGBase.AddSound("Weapon_AK_Alpha.Magin", "weapons/insurgency_s_ak_alpha/ak74_magin.wav")
	UGBase.AddSound("Weapon_AK_Alpha.ROF", "weapons/insurgency_s_ak_alpha/ak47_fireselect_1.wav")

-- M4
	UGBase.AddSoundFire("M4.Single", "weapons/SlaYeR's M4/Shoot_Unsil.wav")
	UGBase.AddSoundFire("M4.Silenced", "weapons/SlaYeR's M4/Shoot.wav")
	UGBase.AddSound("M4.Deploy", "weapons/SlaYeR's M4/Deploy.wav")
	UGBase.AddSound("M4.Shoulder", "weapons/SlaYeR's M4/Shoulder.wav")
	UGBase.AddSound("M4.Foley", "weapons/SlaYeR's M4/Foley.wav")
	UGBase.AddSound("M4.Magout", "weapons/SlaYeR's M4/Magout.wav")
	UGBase.AddSound("M4.Magout2", "weapons/SlaYeR's M4/Magout2.wav")
	UGBase.AddSound("M4.BoltBack", "weapons/SlaYeR's M4/BoltBack.wav")
	UGBase.AddSound("M4.BoltForward", "weapons/SlaYeR's M4/BoltForward.wav")
	UGBase.AddSound("M4.Magin", "weapons/SlaYeR's M4/Magin.wav")
	UGBase.AddSound("M4.Maginsert", "weapons/SlaYeR's M4/Maginsert.wav")
	UGBase.AddSound("M4.Boltrelease", "weapons/SlaYeR's M4/Boltrelease.wav")
	UGBase.AddSound("M4.Sil_on", "weapons/SlaYeR's M4/Sil_on.wav")
	UGBase.AddSound("M4.Sil_off", "weapons/SlaYeR's M4/Sil_off.wav")
