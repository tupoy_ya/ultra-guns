AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.Editable = false
ENT.Spawnable = false

if SERVER then
	function ENT:Initialize()
		self:SetModel("models/weapons/w_eq_fraggrenade_thrown.mdl")
		self:PhysicsInit(6)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:SetModelScale(1)
		self:SetSolid(6)
		self:SetUseType(SIMPLE_USE)

		local phys = self:GetPhysicsObject()
		if phys:IsValid() then
			phys:Wake()
		end
	end

	function ENT:Think()
		if self.TimeCT < CurTime() then
			local explode = ents.Create( "env_explosion" )
			explode:SetPos(self:GetPos())
			explode:Spawn()
			explode:SetKeyValue("iMagnitude", self.iMagnitude)
			explode:Fire("Explode")

			self:Remove()
		end
	end

	function ENT:PhysicsCollide(data, phys)
		if data.Speed > 8 and data.DeltaTime > 0.1 then
			self:EmitSound("Flashbang.Bounce", 75, 100)
		end
		
		phys:SetVelocity(phys:GetVelocity() * 0.5)
	end
end