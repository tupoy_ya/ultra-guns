-- https://gamebanana.com/mods/217342

SWEP.Base = "ug_base"
SWEP.Category = "Ultra Guns"
SWEP.PrintName = "THOMPSON"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""
SWEP.Slot = 2

SWEP.ViewModelFOV	= 65
SWEP.ViewModelFlip	= false

SWEP.ViewModel		= "models/weapons/v_thompson_m1928_ug.mdl"
SWEP.WorldModel		= "models/weapons/w_thompson_m1928_ug.mdl"

SWEP.Spawnable		= true
SWEP.AdminOnly		= false

SWEP.Primary.ClipSize		= 30
SWEP.Primary.DefaultClip	= 9999
SWEP.Primary.TakeBullets		= 1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= ".45 ACP"
SWEP.Primary.Delay  		= 60 / 800
SWEP.Primary.Spread = 9.5
SWEP.Primary.Damage = 25
SWEP.Primary.Recoil = .345

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.CSMuzzleX = false
SWEP.CSMuzzleFlashes = false

SWEP.WeaponType = "assault_rifle" -- this isn't assault rifle but... yes.
SWEP.HoldType = "smg"

SWEP.Ignore__sv_defaultdeployspeed = true
SWEP.HasIdleAnimation = false
SWEP.ShakeMultiplier = 8
SWEP.MaxShake = 5
SWEP.MouseSensivityInZoom = 0.8
SWEP.FireSound = "Weapon_m1928a1.Single"
SWEP.Load_1_Additional_Gun_Cartidge = true
SWEP.ReloadSpeedMultiplier = 1.7
SWEP.ForceCalcViewMultiplier = 2.5
SWEP.AttackSpeedMultiplier = 1.1
SWEP.AutoReload = false
SWEP.SpreadEyeAnglesMultiplier = .085
SWEP.ShouldPlayFireAnimInZoom = false
SWEP.AttachmentForReloadViewBob = 1
SWEP.ReloadViewBobMultiplier = 0.8
SWEP.EnableViewBobOnDeploy = true
SWEP.EnableViewBobOnReloading = true
SWEP.FireTypeInZoom = 0
SWEP.DeployTimeMultiplier = 11.5
SWEP.DeployTime = 0.7
SWEP.CustomTimeFirstDraw = 1.5
SWEP.MagazineInsertedCycle = 0.65
SWEP.Firemodes = {"default", "semi", "safe"}
SWEP.UnitsWhereWillMinimalDamage = 2300
SWEP.HideMaterials = {
	["models/weapons/v_models/hands/us_sleeve_vip"] = true
}

SWEP.WorldModelScale = 0.7
SWEP.WorldModelOrigin = Vector(-0.5, -1, -1.25)
SWEP.WorldModelRotate = Angle(7, -1, 0)

SWEP.RunIronSightsPos = Vector(4, 2, -1)
SWEP.RunIronSightsAng = Angle(-11, 38.931, 0)

SWEP.AlternativePos = Vector(1, 0.5, -0.54)
SWEP.AlternativeAng = Angle(0, 0, 0)

SWEP.ZoomIronSightsPos =  Vector(-2.241, 0, 1)
SWEP.ZoomIronSightsAng = Angle(0.28, 0, 0)

SWEP.Animations = {
	["reload"] = "foregrip_reload",
	["reload_empty"] = "foregrip_reloadempty",
	["first_draw"] = "foregrip_ready",
	["draw"] = "foregrip_draw"
}
