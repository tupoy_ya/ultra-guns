-- https://gamebanana.com/mods/207445

SWEP.Base = "ug_base"
SWEP.Category = "Ultra Guns"
SWEP.PrintName = "ДВУСТВОЛКА ДЕДОВСКАЯL"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""
SWEP.Slot = 3

SWEP.ViewModelFOV	= 80
SWEP.ViewModelFlip	= false
SWEP.ViewModel		= "models/weapons/v_shot_m3super90.mdl"
SWEP.WorldModel		= "models/weapons/w_shot_m3super90.mdl"

SWEP.Spawnable		= true
SWEP.AdminOnly		= false

SWEP.Primary.ClipSize		= 2
SWEP.Primary.DefaultClip	= 9999
SWEP.Primary.CountBullets		= 12
SWEP.Primary.TakeBullets		= 2
SWEP.Primary.Spread = 0.04
SWEP.Primary.Recoil = 0.45
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "12 Gauge"
SWEP.Primary.Delay  		= 60 / 120
SWEP.Primary.Damage = 26

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.CSMuzzleX = false
SWEP.CSMuzzleFlashes = false

SWEP.WeaponType = "shotgun"
SWEP.HoldType = "shotgun"

SWEP.Ignore__sv_defaultdeployspeed = true
SWEP.HasIdleAnimation = true
SWEP.ShakeMultiplier = 24
SWEP.ForceCalcViewMultiplier = 4
SWEP.MouseSensivityInZoom = 0.8
SWEP.FireSound = "dbarrel.fire"
SWEP.Load_1_Additional_Gun_Cartidge = false
SWEP.EnableViewBobOnDeploy = true
SWEP.ReloadSpeedMultiplier = 1.7
SWEP.AttackSpeedMultiplier = 0.8
SWEP.AutoReload = true
SWEP.PushShakeOnZoom = true
SWEP.MagazineInsertedCycle = 0.6
SWEP.EnableViewBobOnReloading = true
SWEP.UnitsWhereWillMinimalDamage = 2250
SWEP.DisableRunningAnimation = true


SWEP.WorldModelScale = 1.2
SWEP.WorldModelOrigin = Vector(-1, 3, -1.5)
SWEP.WorldModelRotate = Angle(12, 0, 0)

SWEP.RunIronSightsPos = Vector(3, 0, -7.25)
SWEP.RunIronSightsAng = Angle(21, 37.931, 0)

SWEP.AlternativePos = Vector(0, -0.2, 0.84)
SWEP.AlternativeAng = Angle(0, 0, 0)

SWEP.ZoomIronSightsPos = Vector(5.75, 0, 3.3)
SWEP.ZoomIronSightsAng = Angle(0, 0, 0)
