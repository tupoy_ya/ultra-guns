-- https://gamebanana.com/mods/208333

SWEP.Base = "ug_base"
SWEP.Category = "Ultra Guns"
SWEP.PrintName = "W1200"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""
SWEP.Slot = 3

SWEP.ViewModelFOV	= 70
SWEP.ViewModelFlip	= false
SWEP.ViewModel		= "models/weapons/v_w1200_ug.mdl"
SWEP.WorldModel		= "models/weapons/w_w1200_ug.mdl"

SWEP.Spawnable		= true
SWEP.AdminOnly		= false

SWEP.Primary.ClipSize		= 7
SWEP.Primary.DefaultClip	= 9999
SWEP.Primary.CountBullets		= 10
SWEP.Primary.TakeBullets		= 1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "12 Gauge"
SWEP.Primary.Delay  		= 60 / 80
SWEP.Primary.Spread = 0.025
SWEP.Primary.Recoil = .48
SWEP.Primary.Damage = 18

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.CSMuzzleX = false
SWEP.CSMuzzleFlashes = false

SWEP.WeaponType = "shotgun_with_slow_reload"
SWEP.HoldType = "shotgun"

SWEP.Ignore__sv_defaultdeployspeed = true
SWEP.HasIdleAnimation = false
SWEP.ShakeMultiplier = 13
SWEP.ForceCalcViewMultiplier = 4
SWEP.MouseSensivityInZoom = 0.8
SWEP.FireSound = "m3.fire"
SWEP.Load_1_Additional_Gun_Cartidge = true
SWEP.ReloadSpeedMultiplier = 1.2
SWEP.AttackSpeedMultiplier = 1
SWEP.AutoReload = false
SWEP.MDM = .45
SWEP.AttachmentForReloadViewBob = 1
SWEP.DeployTimeMultiplier = 1.3
SWEP.ShouldPlayFireAnimInZoom = true
SWEP.EnableViewBobOnDeploy = true
SWEP.PushShakeOnZoom = true
SWEP.EnableViewBobOnReloading = true
SWEP.UnitsWhereWillMinimalDamage = 2250
SWEP.DEBUG_PrintMaterials = false
SWEP.HideMaterials = {
	["models/weapons/v_models/hands/ts_watch_difffuse"] = true,
	["models/weapons/v_models/hands/ts_belt_difffuse"] = true,
	["models/weapons/v_models/hands/ts_watch_difffuse"] = true
} -- ДА Я В РОТ ЕБАЛ ЭТИ ЧАСЫ БЛЯТЬ

SWEP.WorldModelScale = 0.82
SWEP.WorldModelOrigin = Vector(-0.5, 0.5, -0.5)
SWEP.WorldModelRotate = Angle(6, 0, 0)

SWEP.RunIronSightsPos = Vector(3, 0, -7.25)
SWEP.RunIronSightsAng = Angle(21, 37.931, 0)

SWEP.AlternativePos = Vector(0, 0, -0.84)
SWEP.AlternativeAng = Angle(0, 0, 0)

SWEP.ZoomIronSightsPos = Vector(-2.641, 0, 1.08)
SWEP.ZoomIronSightsAng = Angle(0.432, 0, 0)

SWEP.Animations = {
	["start_reload"] = "start_reload",
	["after_reload"] = "after_reload",
	["reload"] = "insert"
}
