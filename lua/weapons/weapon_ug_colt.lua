-- https://gamebanana.com/mods/207331

SWEP.Base = "ug_base"
SWEP.Category = "Ultra Guns"
SWEP.PrintName = "Colt King Cobra .357"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""
SWEP.Slot = 1

SWEP.ViewModelFOV	= 70
SWEP.ViewModelFlip	= false
SWEP.ViewModel		= "models/weapons/v_colt_ug.mdl"
SWEP.WorldModel		= "models/weapons/w_colt_ug.mdl"

SWEP.Spawnable		= true
SWEP.AdminOnly		= false

SWEP.Primary.ClipSize		= 6
SWEP.Primary.DefaultClip	= 9999
SWEP.Primary.TakeBullets		= 1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= ".357 Magnum"
SWEP.Primary.Delay  		= 60 / 130
SWEP.Primary.Spread = 5
SWEP.Primary.Damage = 70
SWEP.Primary.Recoil = .4

SWEP.CSMuzzleX = false
SWEP.CSMuzzleFlashes = false

SWEP.WeaponType = "pistol"
SWEP.HoldType = "revolver"

SWEP.Ignore__sv_defaultdeployspeed = true
SWEP.HasIdleAnimation = false
SWEP.ShakeMultiplier = 12
SWEP.MaxShake = 5
SWEP.MouseSensivityInZoom = 0.8
SWEP.FireSound = "Weapon_revolver.Fire"
SWEP.Load_1_Additional_Gun_Cartidge = false
SWEP.ReloadSpeedMultiplier = 1.7
SWEP.ForceCalcViewMultiplier = 2
SWEP.AttackSpeedMultiplier = 1.2
SWEP.AutoReload = false
SWEP.SpreadEyeAnglesMultiplier = .085
SWEP.ShouldPlayFireAnimInZoom = true
SWEP.AttachmentForReloadViewBob = 2
SWEP.ReloadViewBobMultiplier = 0.1
SWEP.EnableViewBobOnDeploy = true
SWEP.EnableViewBobOnReloading = true
SWEP.FireTypeInZoom = 1
SWEP.MagazineInsertedCycle = 0.6
SWEP.DisableDoubleGuns = false
SWEP.MuzzleFlashUpMult = 0.8
SWEP.DeployTime = 0.4
SWEP.CustomTimeFirstDraw = 2.5
SWEP.DeployPlaybackRate = 1

SWEP.WorldModelOrigin = Vector(-0.5, -0.3, -0.5)
SWEP.WorldModelRotate = Angle(5, 0, 0)

SWEP.RunIronSightsPos = Vector(1, 0, -0.2)
SWEP.RunIronSightsAng = Angle(-14, 22, 0)

SWEP.AlternativePos = Vector(1, 0, -0.84)
SWEP.AlternativeAng = Angle(0, 0, 0)

SWEP.ZoomIronSightsPos = Vector(-1.854, 0.8, -0.112)
SWEP.ZoomIronSightsAng = Angle(0, 0, 0)

SWEP.Animations = {
	["reload_empty"] = "base_reload_speed",
	["reload"] = "base_reload_speed",
	["first_draw"] = "base_ready",
	["draw"] = "base_draw",
	["shoot"] = "base_fire"
}
