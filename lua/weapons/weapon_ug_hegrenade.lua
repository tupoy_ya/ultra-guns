-- https://gamebanana.com/mods/208333

SWEP.Base = "ug_base"
SWEP.Category = "Ultra Guns"
SWEP.PrintName = "HE Grenade"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""
SWEP.Slot = 3

SWEP.ViewModelFlip	= true
SWEP.ViewModel		= "models/weapons/v_eq_fraggrenade.mdl"
SWEP.WorldModel		= "models/weapons/w_eq_fraggrenade.mdl"

SWEP.Spawnable		= true
SWEP.AdminOnly		= false

SWEP.Primary.ClipSize		= 1
SWEP.Primary.DefaultClip	= 99999
SWEP.Primary.TakeBullets		= 1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "Grenade"
SWEP.Primary.Damage = 125

SWEP.CSMuzzleX = false
SWEP.CSMuzzleFlashes = false

SWEP.WeaponType = "grenade"
SWEP.HoldType = "grenade"

SWEP.Ignore__sv_defaultdeployspeed = true
SWEP.DeployPlaybackRate = 1
SWEP.CustomTimeFirstDraw = 0.7
SWEP.DeployTime = 0.7

SWEP.RunIronSightsPos = Vector(0, 0, 0)
SWEP.RunIronSightsAng = Angle(0, 0, 0)
