-- https://gamebanana.com/mods/211486

SWEP.Base = "ug_base"
SWEP.Category = "Ultra Guns"
SWEP.PrintName = "FAL"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""
SWEP.Slot = 2

SWEP.ViewModelFOV	= 60
SWEP.ViewModelFlip	= false

SWEP.ViewModel		= "models/weapons/v_rif_fnfal_ug.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_fnfal_ug.mdl"

SWEP.Spawnable		= true
SWEP.AdminOnly		= false

SWEP.Primary.ClipSize		= 20
SWEP.Primary.DefaultClip	= 9999
SWEP.Primary.TakeBullets		= 1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "7.62x51MM"
SWEP.Primary.Delay  		= 60 / 650
SWEP.Primary.Spread = 12
SWEP.Primary.Damage = 36
SWEP.Primary.Recoil = .445

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.CSMuzzleX = false
SWEP.CSMuzzleFlashes = false

SWEP.WeaponType = "assault_rifle"
SWEP.HoldType = "ar2"

SWEP.Ignore__sv_defaultdeployspeed = true
SWEP.DeployPlaybackRate = 1
SWEP.HasIdleAnimation = false
SWEP.ShakeMultiplier = 8
SWEP.MouseSensivityInZoom = 0.8
SWEP.FireSound = nil
SWEP.Load_1_Additional_Gun_Cartidge = true
SWEP.ReloadSpeedMultiplier = 1.15
SWEP.ForceCalcViewMultiplier = 2.5
SWEP.AttackSpeedMultiplier = .85
SWEP.AutoReload = false
SWEP.SpreadEyeAnglesMultiplier = .085
SWEP.ShouldPlayFireAnimInZoom = false
SWEP.AttachmentForReloadViewBob = 2
SWEP.ReloadViewBobMultiplier = 0.3
SWEP.EnableViewBobOnDeploy = true
SWEP.EnableViewBobOnReloading = true
SWEP.FireTypeInZoom = 0
SWEP.FireSound = "Weapon_fnfal2.Single"
SWEP.MagazineInsertedCycle = 0.64
SWEP.DisableViewBobInZoom = true
SWEP.DeployTime = 1.2

SWEP.WorldModelScale = 0.8
SWEP.WorldModelRotate = Angle(4, 0, 0)

SWEP.RunIronSightsPos = Vector(2.5, 1, -1)
SWEP.RunIronSightsAng = Angle(-11, 32.931, 0)

SWEP.AlternativePos = Vector(0, -0.2, -0.98)
SWEP.AlternativeAng = Angle(0, 0, 0)

SWEP.ZoomIronSightsPos = Vector(-2.925, 1, 0.56)
SWEP.ZoomIronSightsAng = Angle(0.561, 0, 0)

