-- https://gamebanana.com/mods/207198

SWEP.Base = "ug_base"
SWEP.Category = "Ultra Guns"
SWEP.PrintName = "Mauser C96"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""
SWEP.Slot = 1

SWEP.ViewModelFOV	= 70
SWEP.ViewModelFlip	= false
SWEP.ViewModel		= "models/weapons/v_mauser_ug.mdl"
SWEP.WorldModel		= "models/weapons/w_mauser_ug.mdl"

SWEP.Spawnable		= true
SWEP.AdminOnly		= false

SWEP.Primary.ClipSize		= 10
SWEP.Primary.DefaultClip	= 9999
SWEP.Primary.TakeBullets		= 1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= ".45 ACP"
SWEP.Primary.Delay  		= 60 / 400
SWEP.Primary.Spread = 7
SWEP.Primary.Damage = 42
SWEP.Primary.Recoil = .235

SWEP.CSMuzzleX = false
SWEP.CSMuzzleFlashes = false

SWEP.WeaponType = "pistol"
SWEP.HoldType = "pistol"

SWEP.Ignore__sv_defaultdeployspeed = true
SWEP.HasIdleAnimation = false
SWEP.ShakeMultiplier = 8
SWEP.MaxShake = 5
SWEP.MouseSensivityInZoom = 0.8
SWEP.FireSound = "Weapon_C96.Fire"
SWEP.Load_1_Additional_Gun_Cartidge = false
SWEP.ReloadSpeedMultiplier = 1.7
SWEP.ForceCalcViewMultiplier = 2
SWEP.AttackSpeedMultiplier = 1
SWEP.AutoReload = false
SWEP.SpreadEyeAnglesMultiplier = .085
SWEP.ShouldPlayFireAnimInZoom = true
SWEP.AttachmentForReloadViewBob = 1
SWEP.ReloadViewBobMultiplier = 0.8
SWEP.EnableViewBobOnDeploy = true
SWEP.EnableViewBobOnReloading = true
SWEP.FireTypeInZoom = 1
SWEP.DeployTime = 0.5
SWEP.CustomTimeFirstDraw = 0.5
SWEP.MagazineInsertedCycle = 0.5
SWEP.DisableDoubleGuns = false
SWEP.MuzzleFlashUpMult = 0.8
SWEP.DeployPlaybackRate = 1

SWEP.WorldModelOrigin = Vector(-0.5, -0.3, -0.5)

SWEP.RunIronSightsPos = Vector(0.5, -2, 0)
SWEP.RunIronSightsAng = Angle(-14, 0, 0)

SWEP.AlternativePos = Vector(-2, -0.2, -0.84)
SWEP.AlternativeAng = Angle(0, 0, 0)

SWEP.ZoomIronSightsPos = Vector(-2.631, 0, 0.65)
SWEP.ZoomIronSightsAng = Angle(1.062, 0, 0)

SWEP.Animations = {
	["shoot_empty"] = "base_firelast",
	["reload_empty"] = "base_reload_empty_clip",
	["reload"] = "base_reload_clip"
}
