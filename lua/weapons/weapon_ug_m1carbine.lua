-- https://gamebanana.com/mods/211487

SWEP.Base = "ug_base"
SWEP.Category = "Ultra Guns"
SWEP.PrintName = "M1 КАРАБИН"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""
SWEP.Slot = 2

SWEP.ViewModelFOV	= 70
SWEP.ViewModelFlip	= false

SWEP.ViewModel		= "models/weapons/v_rif_m1c_ug.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_m1c_ug.mdl"

SWEP.Spawnable		= true
SWEP.AdminOnly		= false

SWEP.Primary.ClipSize		= 15
SWEP.Primary.DefaultClip	= 9999
SWEP.Primary.TakeBullets		= 1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "7.62x33MM"
SWEP.Primary.Delay  		= 60 / 300
SWEP.Primary.Spread = 9.5
SWEP.Primary.Damage = 40
SWEP.Primary.Recoil = .375

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.CSMuzzleX = false
SWEP.CSMuzzleFlashes = false

SWEP.WeaponType = "assault_rifle"
SWEP.HoldType = "ar2"

SWEP.Ignore__sv_defaultdeployspeed = true
SWEP.HasIdleAnimation = false
SWEP.ShakeMultiplier = 8
SWEP.MouseSensivityInZoom = 0.8
SWEP.FireSound = "Weapon_m1carbine.Single"
SWEP.Load_1_Additional_Gun_Cartidge = true
SWEP.ReloadSpeedMultiplier = 1.5
SWEP.ForceCalcViewMultiplier = 2.5
SWEP.AttackSpeedMultiplier = .85
SWEP.AutoReload = false
SWEP.SpreadEyeAnglesMultiplier = .085
SWEP.ShouldPlayFireAnimInZoom = false
SWEP.AttachmentForReloadViewBob = 2
SWEP.ReloadViewBobMultiplier = 0.2
SWEP.EnableViewBobOnDeploy = true
SWEP.EnableViewBobOnReloading = true
SWEP.FireTypeInZoom = 0
SWEP.DeployTime = 0.5
SWEP.CustomTimeFirstDraw = 1.5
SWEP.MagazineInsertedCycle = 0.7
SWEP.DisableViewBobInZoom = true
SWEP.WorldModelAttachment = 0
SWEP.HideMaterials = {
	["models/weapons/v_models/m1a1/bayonet"] = true,
	["models/weapons/v_models/m1a1/iron_s"] = true,
	["models/weapons/v_models/m1a1/iron_s"] = true
}

SWEP.WorldModelScale = 0.78
SWEP.WorldModelOrigin = Vector(-0.5, 0.5, -0.5)
SWEP.WorldModelRotate = Angle(6, -1, 0)

SWEP.RunIronSightsPos = Vector(4.5, 0, -1.25)
SWEP.RunIronSightsAng = Angle(-11, 38.931, 0)

SWEP.AlternativePos = Vector(0, -0.2, -0.84)
SWEP.AlternativeAng = Angle(0, 0, 0)

SWEP.ZoomIronSightsPos = Vector(-2.487, 0, 1.24)
SWEP.ZoomIronSightsAng = Angle(0, 0, 0)

SWEP.Animations = {
	["reload_empty"] = "base_reloadempty",
	["reload"] = "base_reload",
	["first_draw"] = "base_ready",
	["draw"] = "base_draw"
}
