-- https://gamebanana.com/mods/211664

SWEP.Base = "ug_base"
SWEP.Category = "Ultra Guns"
SWEP.PrintName = "FAMAS"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""
SWEP.Slot = 2

SWEP.ViewModelFOV	= 70
SWEP.ViewModelFlip	= false

SWEP.ViewModel		= "models/weapons/v_rif_famas_ug.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_famas_ug.mdl"

SWEP.Spawnable		= true
SWEP.AdminOnly		= false

SWEP.Primary.ClipSize		= 30
SWEP.Primary.DefaultClip	= 9999
SWEP.Primary.TakeBullets		= 1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "5.56x45MM"
SWEP.Primary.Delay  		= 60 / 925
SWEP.Primary.Spread = 10.5
SWEP.Primary.Damage = 30
SWEP.Primary.Recoil = .625

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.CSMuzzleX = false
SWEP.CSMuzzleFlashes = false

SWEP.WeaponType = "assault_rifle"
SWEP.HoldType = "ar2"

SWEP.Ignore__sv_defaultdeployspeed = true
SWEP.HasIdleAnimation = false
SWEP.ShakeMultiplier = 7.5
SWEP.MaxShake = 7
SWEP.MouseSensivityInZoom = 0.8
SWEP.FireSound = "FC2_Famas.Single"
SWEP.Load_1_Additional_Gun_Cartidge = true
SWEP.ReloadSpeedMultiplier = 1.15
SWEP.ForceCalcViewMultiplier = 2.5
SWEP.AttackSpeedMultiplier = 1.1
SWEP.AutoReload = false
SWEP.SpreadEyeAnglesMultiplier = .085
SWEP.ShouldPlayFireAnimInZoom = false
SWEP.AttachmentForReloadViewBob = 1
SWEP.EnableViewBobOnDeploy = true
SWEP.EnableViewBobOnReloading = true
SWEP.FireTypeInZoom = 0
SWEP.DeployPlaybackRate = 1.2
SWEP.DeployTime = 0.5
SWEP.MagazineInsertedCycle = 0.5
SWEP.Firemodes = {"default", "3burst", "semi", "safe"}

SWEP.WorldModelScale = 0.9
SWEP.WorldModelOrigin = Vector(0, -1.5, -0.3)
SWEP.WorldModelRotate = Angle(10, 0, 0)

SWEP.RunIronSightsPos = Vector(3.5, 1, -1)
SWEP.RunIronSightsAng = Angle(-11, 38.931, 0)

SWEP.AlternativePos = Vector(0, 0.5, -0.84)
SWEP.AlternativeAng = Angle(0, 0, 0)

SWEP.ZoomIronSightsPos =  Vector(-3.32, -4.5, -0.76)
SWEP.ZoomIronSightsAng = Angle(0, 0, 0)

SWEP.Animations = {
	["reload"] = {"reload", "reload_alt", "reload_alt_2"}
}
