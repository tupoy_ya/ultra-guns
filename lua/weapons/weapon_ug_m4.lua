-- https://gamebanana.com/mods/210253

SWEP.Base = "ug_base"
SWEP.Category = "Ultra Guns"
SWEP.PrintName = "M4"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""
SWEP.Slot = 2

SWEP.ViewModelFOV	= 70
SWEP.ViewModelFlip	= false

SWEP.ViewModel		= "models/weapons/v_rif_m4a1_ug.mdl"
SWEP.WorldModel		= "models/weapons/w_rif_m4a1_ug.mdl"
SWEP.WorldModelSilencer		= "models/weapons/w_rif_m4a1_silencer_ug.mdl"

SWEP.Spawnable		= true
SWEP.AdminOnly		= false

SWEP.Primary.ClipSize		= 10
SWEP.Primary.DefaultClip	= 9999
SWEP.Primary.TakeBullets		= 1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "5.56x39MM"
SWEP.Primary.Delay  		= 60 / 600
SWEP.Primary.Spread = 10.75
SWEP.Primary.Damage = 0
SWEP.Primary.Recoil = .455

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.CSMuzzleX = false
SWEP.CSMuzzleFlashes = false

SWEP.WeaponType = "assault_rifle"
SWEP.HoldType = "ar2"

SWEP.Ignore__sv_defaultdeployspeed = true
SWEP.HasIdleAnimation = false
SWEP.ShakeMultiplier = 7.5
SWEP.MaxShake = 7
SWEP.MouseSensivityInZoom = 0.8
SWEP.FireSound = "M4.Single"
SWEP.FireSoundWithSilencer = "M4.Silenced"
SWEP.Load_1_Additional_Gun_Cartidge = true
SWEP.ReloadSpeedMultiplier = 1.2
SWEP.ForceCalcViewMultiplier = 6.5
SWEP.AttackSpeedMultiplier = 1.1
SWEP.AutoReload = false
SWEP.SpreadEyeAnglesMultiplier = .085
SWEP.ShouldPlayFireAnimInZoom = false
SWEP.AttachmentForReloadViewBob = 2
SWEP.ReloadViewBobMultiplier = 0.5
SWEP.EnableViewBobOnDeploy = true
SWEP.EnableViewBobOnReloading = true
SWEP.DisableViewBobInZoom = true
SWEP.FireTypeInZoom = 0
SWEP.DeployPlaybackRate = 1.2
SWEP.MagazineInsertedCycle = 0.45
SWEP.UnitsWhereWillMinimalDamage = 3150
SWEP.Firemodes = {"default", "semi", "safe"}
SWEP.HasSilencer = true
SWEP.DeployTime = 0.6

SWEP.WorldModelScale = 0.8
SWEP.WorldModelOrigin = Vector(-0.6, 1, -1)
SWEP.WorldModelRotate = Angle(10, -1, 0)

SWEP.RunIronSightsPos = Vector(3, 1, -1)
SWEP.RunIronSightsAng = Angle(-11, 38.931, 0)

SWEP.AlternativePos = Vector(2, 0, -0.8)
SWEP.AlternativeAng = Angle(0, 0, 0)

SWEP.ZoomIronSightsPos = Vector(-2.353, -0.5, 0.259)
SWEP.ZoomIronSightsAng = Angle(0, 0, 0)

SWEP.Animations = {
	["attach_sil"] = "attach_sil"
}
