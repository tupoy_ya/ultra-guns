local Lerp, math, CurTime, draw, surface, string, GetConVar = Lerp, math, CurTime, draw, surface, string, GetConVar
local angle = FindMetaTable("Angle")
local RotateAroundAxis = angle.RotateAroundAxis

UGBase.CVARCreate("ug_displaycrosshair", 1, "Display crosshair?", "Crosshair parts")

UGBase.CVARCreate("ug_enableleftcross", 1, "Crosshair: Left cross", "Crosshair parts")
UGBase.CVARCreate("ug_enablerightcross", 1, "Crosshair: Right cross", "Crosshair parts")
UGBase.CVARCreate("ug_enableuppercross", 1, "Crosshair: Upper cross", "Crosshair parts")
UGBase.CVARCreate("ug_enablelowercross", 1, "Crosshair: Lower cross", "Crosshair parts")
UGBase.CVARCreate("ug_enabledot", 1, "Crosshair: Dot", "Crosshair parts")

CreateClientConVar("ug_outlinecrosshair", "255 255 255", true, false)
CreateClientConVar("ug_inlinecrosshair", "0 0 0", true, false)

UGBase.CVARCreate("ug_usecustomcameramovement", 1, "Enable custom camera movement?", "Camera")
UGBase.CVARCreate("ug_usedrunkcamera", 0, "Enable drunk camera?", "Camera")

UGBase.CVARChangeCallback("ug_usedrunkcamera", function(val)
	if tobool(val) then
		UGBase.Msg("lol good luck")
	end
end)

UGBase.CVARCreate("ug_useshake", 1, "Enable shaking when gun is shooting?", "Camera")
UGBase.CVARCreate("ug_useblur", 1, "Enable Blur?", "Misc.")

UGBase.CVARCreate("ug_fov", 90, "Camera", "FOV", "slider", 25, 120)
UGBase.CVARCreate("ug_vmfov", 70, "View Model", "FOV", "slider", 25, 120)

surface.CreateFont("ug_FiremodeFont", {font = "Roboto", size = 16, antialias = true, extended = true})
surface.CreateFont("ug_SENTFont", {font = "Roboto", size = 72, antialias = true, extended = true})
surface.CreateFont("ug_ScopeFont", {font = "Roboto", size = 11, antialias = true, extended = true})
surface.CreateFont("ug_SelectHUD", {font = "Roboto", size = ScreenScale(10.5), antialias = true, extended = true})
surface.CreateFont("ug_Gta_Money", {font = "Pricedown", size = ScreenScale(20), antialias = true, extended = true})

include("shared.lua")

SWEP.Slot = 0
SWEP.SlotPos = 0
SWEP.DrawAmmo = true
SWEP.DrawCrosshair = false
SWEP.DrawWeaponInfoBox = false
SWEP.BounceWeaponIcon = false
SWEP.SwayScale = 0
SWEP.BobScale = 0

SWEP.RenderGroup = RENDERGROUP_TRANSLUCENT

SWEP.RunIronSightsPos = Vector(0, 0, 0)
SWEP.RunIronSightsAng = Angle(-11, 27.931, 0)

SWEP.ZoomIronSightsPos = Vector(-3.11, 0, 1.11)
SWEP.ZoomIronSightsAng = Angle(0, 0, 0)

SWEP.MeleePos = Vector(6.692, -1, -7.165)
SWEP.MeleeAng = Angle(24.913, 41.812, 0)

SWEP.LerpedZoomIronSightsPos = Vector()
SWEP.LerpedZoomIronSightsAng = Angle()

SWEP.LerpedRunIronSightsPos = Vector()
SWEP.LerpedRunIronSightsAng = Angle()

SWEP.LerpedAlternativePos = Vector()
SWEP.LerpedAlternativeAng = Angle()

function SWEP:LerpVector(delta, start, finish)
	delta = delta > 1 and 1 or delta
	
	start.x = start.x + delta * (finish.x - start.x)
	start.y = start.y + delta * (finish.y - start.y)
	start.z = start.z + delta * (finish.z - start.z)
	
	return start
end

function SWEP:LerpAngle(delta, start, finish)
	delta = delta > 1 and 1 or delta

	start.p = start.p + delta * (finish.p - start.p)
	start.y = start.y + delta * (finish.y - start.y)
	start.r = start.r + delta * (finish.r - start.r)

	return start
end

local erp, erp_to_default, lerp = 0, {}, 0
local time, nextthink = 0, 0

local mousex, mousey = 0, 0
local lerpedx, lerpedy = 0, 0

local LerpedCalcViewT = {0, 0}

local alphaZoom, alphaZoom2 = 0, 0
local gmodScope = Material("gmod/scope")

local motionblur, zoom = 0, 0

local shake, TEST = 0, 0
local TEST2, normal = Vector(), Vector()
local vector_zero, angle_zero, lerpednec, lerpedright = Vector(), Angle(), 0, 0

local clr_white, clr_black = color_white, color_black
local color_white, color_black = color_white, color_black
local backup_color_white, backup_color_black = Color(255, 255, 255), Color(0, 0, 0)

local outline_color = GetConVar("ug_outlinecrosshair")

function SWEP:IsScopeReady()
	return alphaZoom > 1.08
end

hook.Add("StartCommand", "ug_StartCommand", function(ply, ucmd)
	local eyeangles, FT = ply:EyeAngles(), FrameTime()

	local self = ply:GetActiveWeapon()
	if not self:IsValid() then
		lerpedx, lerpedy = Lerp(FT * 16, lerpedx, 0), Lerp(FT * 16, lerpedy, 0)
		return
	end

	if ply:GetObserverTarget():IsValid() then
		lerpedx, lerpedy, self.FOVInZoom = Lerp(FT * 16, lerpedx, 0), Lerp(FT * 16, lerpedy, 0), Lerp(FT * 16, self.FOVInZoom or 0, 0)
		return
	end

	mousex, mousey = math.Remap(ucmd:GetMouseX(), 0, 100, 0, 1), math.Remap(ucmd:GetMouseY(), 0, 100, 0, 1)
	lerpedx = Lerp(FT * 8, lerpedx, math.Clamp(mousex * .5, -2, 2))

	if eyeangles.p <= 88 and eyeangles.p >= -88 then
		lerpedy = Lerp(FT * 8, lerpedy, math.Clamp(mousey * .5, -2, 2))
	else
		lerpedy = Lerp(FT * 8, lerpedy, 0)
	end

	if self.IsUGBase and self.FOVInZoom ~= ucmd:GetMouseWheel() and ucmd:GetMouseWheel() ~= 0 and self:GetNextPrimaryFire() < CurTime() then
		self.FOVInZoom = math.Clamp(self.FOVInZoom - ucmd:GetMouseWheel(), 0, 10)
	end
end)

function SWEP:DrawWorldModel(flags)
	local ply = self:GetOwner()

	if not ply:IsValid() then
		self:DrawModel(flags)

		return
	end

	if self.WeaponType == "grenade" and ply:GetAmmoCount(self:GetPrimaryAmmoType()) and self:Clip1() == 0 then
		return
	end

	ply:SetupBones()

	local hand = ply:LookupBone("ValveBiped.Bip01_R_Hand")

    if not hand then
        return
    end

	local pos, ang = ply:GetBonePosition(hand)
	
	ang:RotateAroundAxis(ang:Forward(), 180)

	if self.WorldModelOrigin then
		pos = pos + ang:Right() * self.WorldModelOrigin.x + ang:Forward() * self.WorldModelOrigin.y + ang:Up() * self.WorldModelOrigin.z
	end
	
	if self.WorldModelRotate then
		ang:RotateAroundAxis(ang:Right(), self.WorldModelRotate.p)
		ang:RotateAroundAxis(ang:Up(), self.WorldModelRotate.y)
		ang:RotateAroundAxis(ang:Forward(), self.WorldModelRotate.r)
	end

	self:SetPos(pos)
	self:SetAngles(ang)

	if ply:IsPlayer() or (ply.IsBot and ply:IsBot()) then
		self:SetRenderOrigin(pos)
		self:SetRenderAngles(ang)
	end

	self:SetupBones()
	self:SetModelScale(self.WorldModelScale or 1)
	self:DrawModel(flags)

	if self:GetDouble() then
		local hand = ply:LookupBone("ValveBiped.Bip01_L_Hand")
		
		if not hand then
			return
		end

		local pos, ang = ply:GetBonePosition(hand)

		if self.WorldModelOrigin then
			pos = pos - ang:Right() * self.WorldModelOrigin.x - ang:Forward() * self.WorldModelOrigin.y + ang:Up() * self.WorldModelOrigin.z
		end
		
		if self.WorldModelRotate then
			ang:RotateAroundAxis(ang:Up(), self.WorldModelRotate.y)
			ang:RotateAroundAxis(ang:Forward(), self.WorldModelRotate.r)
		end

		if not IsValid(self.WorldModelSecondWeapon) then
			self.WorldModelSecondWeapon = ClientsideModel(self.WorldModel)
			self.WorldModelSecondWeapon:SetNoDraw(true)
		end

		self.WorldModelSecondWeapon:SetPos(pos)
		self.WorldModelSecondWeapon:SetAngles(ang)

		if ply:IsPlayer() or ply:IsBot() then
			self:SetRenderOrigin(pos)
			self:SetRenderAngles(ang)
		end

		self.WorldModelSecondWeapon:SetupBones()
		self.WorldModelSecondWeapon:SetModelScale(self.WorldModelScale or 1)
		self.WorldModelSecondWeapon:DrawModel(flags)
	end
end

SWEP.DrawWorldModelTranslucent = SWEP.DrawWorldModel

function SWEP:HUDShouldDraw(element)
	if self.UseScope and self:GetZoom() and element == "CHudWeaponSelection" then
		return false
	end

	return true
end

local cDraw = {}

function SWEP:CustomAmmoDisplay()
	cDraw.PrimaryClip = self:Clip1()
	cDraw.PrimaryAmmo = self:Ammo1()
	cDraw.SecondaryAmmo = self:GetDoubleClip()

	return cDraw
end

local color_white, color_black, color_red = color_white, color_black, Color(210, 0, 0)

function SWEP:DrawWeaponSelection(x, y, w, h, alpha)
	surface.SetFont("ug_SelectHUD")

	local clip1, maxclip1 = self:Clip1(), self:GetMaxClip1()
	local text = string.format("%s\n%s / %i", self.PrintName or "Ultra Guns", (self.Load_1_Additional_Gun_Cartidge and (clip1 == maxclip1 + 1 and maxclip1 .. " + 1") or clip1), maxclip1)
	local tW, tH = surface.GetTextSize(text)

	surface.SetAlphaMultiplier(alpha / 255)
		draw.DrawText(text, "ug_SelectHUD", x + w / 2 + 2, y + h / 2 - tH / 2 + 2, clr_black, 1)

		if clip1 <= 0 then
			draw.DrawText(text, "ug_SelectHUD", x + w / 2, y + h / 2 - tH / 2, color_red, 1)
		else
			draw.DrawText(text, "ug_SelectHUD", x + w / 2, y + h / 2 - tH / 2, clr_white, 1)
		end
	surface.SetAlphaMultiplier(1)
end

if outline_color then
	local color = string.Explode(" ", outline_color:GetString())

	backup_color_white = Color(color[1] or 255, color[2] or 255, color[3] or 255)
end

UGBase.CVARChangeCallback("ug_outlinecrosshair", function(val)
	local color = string.Explode(" ", val)

	backup_color_white = Color(color[1] or 255, color[2] or 255, color[3] or 255)
end)

local inline_color = GetConVar("ug_inlinecrosshair")

if inline_color then
	local color = string.Explode(" ", inline_color:GetString())

	color_black = Color(color[1] or 0, color[2] or 0, color[3] or 0)
end

UGBase.CVARChangeCallback("ug_inlinecrosshair", function(val)
	local color = string.Explode(" ", val)

	color_black = Color(color[1] or 0, color[2] or 0, color[3] or 0)
end)

local lerpedShootInScope, lerpedCrosshair = 0, 0

function SWEP:GetTracerOrigin()
	if self.IsSecondWeaponShoot and IsValid(self.SecondWeapon) then
		local ply = self:GetOwner()

		if not ply:IsValid() then
			return
		end

		local attach = self.SecondWeapon:GetAttachment(1)

		if attach then
			return attach.Pos
		end
	end
end

function SWEP:DrawHUD()
	local FT, CT = FrameTime(), CurTime()

	if self.UseScope and self:GetZoom() then
		alphaZoom = Lerp(FT * 12, alphaZoom, 1.1)
	else
		alphaZoom = Lerp(FT * 12, alphaZoom, 0)
		alphaZoom2 = 0
	end

	local scrh, scrw = ScrH(), ScrW()

	lerpedShootInScope = Lerp(FT * 16, lerpedShootInScope, math.Clamp((self.EyeAnglesRecoil - SysTime()) * .25 * math.Clamp(self:GetLastShoot() + .75 - CT, 0, 1), 0, .25) * 666)

	if self:IsScopeReady() then
		local w = scrh * 1.25

		alphaZoom2 = Lerp(FT * 8, alphaZoom2, 1.1)

		local x, y = scrw / 2 - w / 2, scrh / 2 - scrh / 2
		local lx, ly = (lerpedx * 256) - LerpedCalcViewT[1] * 64, (lerpedy * 256) - lerpedShootInScope - LerpedCalcViewT[2] * 128

		surface.SetDrawColor(color_black)
		surface.DrawRect(0, 0, x + lx, scrh)
		surface.DrawRect(x + w + lx - 4, 0, scrw - (x + w) + 8 + math.abs(lx) * 2, scrh)
		surface.DrawRect(x + lx, y, w, ly)
		surface.DrawRect(x + lx, scrh + ly, w, 2 + math.abs(ly))

		surface.SetMaterial(gmodScope)
		surface.DrawTexturedRect(x + lx, y + ly, w, scrh)

		surface.DrawLine(0, scrh / 2 + ly, scrw, scrh / 2 + ly)
		surface.DrawLine(scrw / 2 + lx, 0, scrw / 2 + lx, scrh)

		surface.SetDrawColor(0, 0, 0, (1.1 - alphaZoom2) * 255)
		surface.DrawRect(0, 0, scrw, scrh)
	else
		surface.SetDrawColor(0, 0, 0, alphaZoom * 255)
		surface.DrawRect(0, 0, scrw, scrh)
	end

	local cvar = GetConVar("ug_displaycrosshair")

	local ply = self:GetOwner()

	local isreloading = self:GetReloading()
	local current_firemode = self.Firemodes[self:GetFiremode()]

	if cvar and not cvar:GetBool() then
		if self.OldFiremode ~= current_firemode and current_firemode ~= nil then
			self.AlphaHUDF, self.OldFiremode = CT + 1, current_firemode
		end

		surface.SetAlphaMultiplier(math.Clamp(self.AlphaHUDF - CT, 0, 1))
			draw.SimpleText(current_firemode, "ug_FiremodeFont", ScrW() / 2 + 1, ScrH() / 2 + 1 + 25, color_black, 1, 1)
			draw.SimpleText(current_firemode, "ug_FiremodeFont", ScrW() / 2, ScrH() / 2 + 25, color_white, 1, 1)
		surface.SetAlphaMultiplier(1)

		return
	end

	draw.SimpleTextOutlined("$" .. ply:GetNWInt("Player_Money", 0), "ug_Gta_Money", ScrW() / 1.1, ScrH() / 9, Color(29, 105, 49), 1, 1, 4, Color(0,0,0))

	local vm = ply:GetViewModel()
	local shootpos, FT = ply:GetShootPos(), FrameTime()

	local trace = {
		["start"] = shootpos,
		["endpos"] = shootpos + ply:EyeAngles():Forward() * 16834,
		["filter"] = ply
	}

	local tr = util.TraceLine(trace)
	local pos = tr.HitPos:ToScreen()
	local x, y = math.Round(pos.x), math.Round(pos.y)

	local lastshoot = math.Clamp((self:GetLastShoot() - CT), 0, 1) * 28 * (self:GetSilencer() and 0.8 or 1)

	local double = self:GetDouble() and self:GetDoubleClip() or 0

	if self:Clip1() + double < self:GetMaxClip1() * .5 then
		local this = 1 - math.Clamp((self:Clip1() + double) / (self:GetMaxClip1() * .35), 0, 1)

		color_white = Color(backup_color_white.r - this * 25, backup_color_white.g - this * 255, backup_color_white.b - this * 255, 255)
	else
		color_white = backup_color_white
	end

	local plX, plY = 0, 0
	
	if self.DebugCrosshair then
		draw.RoundedBox(0, x, y, 2, 2, clr_white)
	end

	local trd = (self.WeaponType == "sniper_rifle" and self:GetZoom() and ply:ShouldDrawLocalPlayer())

	if (self.WeaponType ~= "sniper_rifle" or trd) and self.WeaponType ~= "grenade" then
		surface.SetAlphaMultiplier(self.LerpedCrosshairAlpha)

			local w, h = 2, 2
			local cvar = GetConVar("ug_enabledot")

			if cvar and cvar:GetBool() then
				draw.RoundedBox(0, x - (w + 1) / 2, y - (h + 1) / 2, w + 2, h + 2, color_black)
				draw.RoundedBox(0, x - w / 2, y - h / 2, w, h, color_white)
			end

			local w, h = 8, 2

			if self.WeaponType == "shotgun" or self.WeaponType == "shotgun_with_slow_reload" then
				plX, plY = 20 + self.Primary.Spread * 200, self.Primary.Spread * 200
			else
				lastshoot = lastshoot * (self.Primary.Spread or 0) / 10
			end

			local walkspeed = ply:GetWalkSpeed()
			local mult = 1

			if self:GetZoom() and ply:Crouching() then
				mult = .4
			elseif self:GetZoom() then
				mult = .5
			elseif ply:Crouching() then
				mult = .7
			end

			if not ply:OnGround() then
				mult = mult + .35
			end

			if self:GetRunning() then
				mult = mult + .8
			elseif ply:GetVelocity():LengthSqr() > walkspeed * walkspeed then
				mult = mult + .35
			end

			if self:GetDouble() then
				mult = mult + 2
			end

			lerpedCrosshair = Lerp(FT * 24, lerpedCrosshair, mult)
			lastshoot = (lastshoot * lerpedCrosshair) + 4

			local cvar = GetConVar("ug_enableleftcross")

			if cvar and cvar:GetBool() then
				draw.RoundedBox(0, x - (w + 1) - lastshoot - plX, y - (h + 1) / 2, w + 2, h + 2, color_black)
				draw.RoundedBox(0, x - w - lastshoot - plX, y - h / 2, w, h, color_white)
			end

			local cvar = GetConVar("ug_enablerightcross")

			if cvar and cvar:GetBool() then
				draw.RoundedBox(0, x + lastshoot - 1 + plX, y - (h + 1) / 2, w + 2, h + 2, color_black)
				draw.RoundedBox(0, x + lastshoot + plX, y - h / 2, w, h, color_white)
			end

			local w, h = 2, 8

			local cvar = GetConVar("ug_enablelowercross")

			if cvar and cvar:GetBool() then
				draw.RoundedBox(0, x - (w + 1) / 2, y + lastshoot - 1 + plY, w + 2, h + 2, color_black)
				draw.RoundedBox(0, x - w / 2, y + lastshoot + plY, w, h, color_white)
			end

			local cvar = GetConVar("ug_enableuppercross")

			if cvar and cvar:GetBool() then
				draw.RoundedBox(0, x - (w + 1) / 2, y - h - lastshoot - 1 - plY, w + 2, h + 2, color_black)
				draw.RoundedBox(0, x - w / 2, y - h - lastshoot - plY, w, h, color_white)
			end

		surface.SetAlphaMultiplier(1)
	end

	local current_firemode = self.Firemodes[self:GetFiremode()]

	if self.OldFiremode ~= current_firemode and current_firemode ~= nil then
		self.AlphaHUDF, self.OldFiremode = CT + 1, current_firemode
	end

	surface.SetAlphaMultiplier(math.Clamp(self.AlphaHUDF - CT, 0, 1))
		draw.SimpleText(current_firemode, "ug_FiremodeFont", x + 1, y + lastshoot + plY + 25 + 1, color_black, 1, 1)
		draw.SimpleText(current_firemode, "ug_FiremodeFont", x, y + lastshoot + plY + 25, clr_white, 1, 1)
	surface.SetAlphaMultiplier(1)
end

function SWEP:AdjustMouseSensitivity()
	if self:GetZoom() then
		if self.UseScope then
			return self.MouseSensivityInZoom * math.Clamp(math.abs(self.FOVInZoom * 0.2), 0.2, 1)
		else
			return self.MouseSensivityInZoom
		end
	end
end

function SWEP:CalcViewModelView(_, oldPos, oldAng, _, _)
	local oldAngRight, oldAngForward, oldAngUp = oldAng:Right(), oldAng:Forward(), oldAng:Up()
	local lerpedrun, lerpedzoom = self.LerpedRunIronSightsPos, self.LerpedZoomIronSightsPos
	local vm_flipped, melee_punch, lerped_zooming = self.ViewModelFlip, self.MeleePunch, self.LerpedZooming
	local flip = (vm_flipped and -1 or 1)
	local isrunning = self:GetRunning() and not self:GetPreAttackReady() and not self:GetDouble()

	oldPos = oldPos + oldAngRight * lerpedrun.x + oldAngForward * lerpedrun.y + oldAngUp * lerpedrun.z + oldAngRight * lerpedzoom.x + oldAngForward * lerpedzoom.y + oldAngUp * lerpedzoom.z
		+ oldAngRight * lerp * (isrunning and 1 or 0.6) + oldAngUp * math.abs(lerp) * 1.25 * (self:GetZoom() and -.45 or .65) * (isrunning and 1 or 0.8)
		- oldAngRight * (vm_flipped and -lerpedx or lerpedx) * lerped_zooming + oldAngUp * lerpedy * lerped_zooming
		+ TEST2 + oldAngUp * self.LerpedBreath
		+ (oldAngRight * self.MeleePos.x * flip + oldAngForward * self.MeleePos.y + oldAngUp * self.MeleePos.z) * melee_punch

	oldPos.z = oldPos.z + normal.z

	RotateAroundAxis(oldAng, oldAngUp, (vm_flipped and lerpedx * .5 or (-lerpedx * .5)) * lerped_zooming - TEST * 1.25 * self.LerpedRun + TEST * self.LerpedRun * 4)
	RotateAroundAxis(oldAng, oldAngRight, -lerpedy * .5 * lerped_zooming)
	RotateAroundAxis(oldAng, oldAngForward, TEST * 4)

	RotateAroundAxis(oldAng, oldAngRight, self.LerpedZoomIronSightsAng.p + self.MeleeAng.p * melee_punch + self.LerpedRunIronSightsAng.p)
	RotateAroundAxis(oldAng, oldAngUp, self.LerpedZoomIronSightsAng.y + self.MeleeAng.y * melee_punch * flip + self.LerpedRunIronSightsAng.y)
	RotateAroundAxis(oldAng, oldAngForward, self.LerpedZoomIronSightsAng.r + self.MeleeAng.r * melee_punch + self.LerpedRunIronSightsAng.r)

	if self.AlternativePos then
		oldPos = oldPos + oldAngForward * self.LerpedAlternativePos.x + oldAngRight * self.LerpedAlternativePos.y + oldAng:Up() * self.LerpedAlternativePos.z
	end

	if self.AlternativeAng then
		RotateAroundAxis(oldAng, oldAngRight, self.LerpedAlternativeAng.p)
		RotateAroundAxis(oldAng, oldAngUp, self.LerpedAlternativeAng.y)
		RotateAroundAxis(oldAng, oldAngForward, self.LerpedAlternativeAng.r)
	end

	if self:GetZoom() then
		if self.FireTypeInZoom == 0 then
			oldPos = oldPos - oldAngForward * self.LerpedShoot
		else
			oldPos = oldPos - oldAngForward * self.LerpedShoot * 3 - oldAngUp * self.LerpedShoot * 2
			RotateAroundAxis(oldAng, oldAngRight, self.LerpedShoot * 9)
		end
	end
	
	RotateAroundAxis(oldAng, oldAng:Right(), -self.DoubleReloading)

	RotateAroundAxis(oldAng, oldAngRight, lerpednec)

	return oldPos, oldAng
end

local vec_zero, vector_fuck = Vector(), Vector(-1000, 1000, 0)

UGBase.CVARChangeCallback("ug_usecustomcameramovement", function() TEST2 = Vector() end)

local function find_whole_word(str, word)
	return string.find(str, "%f[^%z%s]" .. word .. "%f[%z%s]")
end

local last_weapon

hook.Add("Think", "ug_Think", function()
	local ply = LocalPlayer()

	if not ply:IsValid() then
		return
	end

	local wep = ply:GetActiveWeapon()

	if not wep:IsValid() then
		return
	end

	if wep ~= last_weapon then
		last_weapon, wep.WeNeedDeployAnim = wep, true
	end
end)

function SWEP:PreDrawViewModel()
	if self:IsScopeReady() then
		render.SetBlend(0)
	end

    local ply = self:GetOwner()

	if not ply:IsValid() then
		return
	end

	local vm = ply:GetViewModel()

	if not vm:IsValid() then
		return
	end

	local materials = vm:GetMaterials()
	local dbg, scope = self.DEBUG_PrintMaterials, self.ScopeMaterial

	for index = 1, #materials do
		local material = materials[index]

		if dbg then
			print(index, material)
		end

		if string.find(material, "_error", 1, true)
			or (self.HideMaterial and self.HideMaterial[material]) then
			vm:SetSubMaterial(index - 1, "engine/occlusionproxy")
		end
	end

	if self:GetDouble() and IsValid(self.SecondWeapon) then
		for i = 1, vm:GetBoneCount() - 1 do
			local name = string.lower(vm:GetBoneName(i))

			if string.StartWith(name, "l_")
				or string.StartWith(name, "left")
				or find_whole_word(name, "l") then
				vm:ManipulateBonePosition(i, vector_fuck)
				vm:ManipulateBoneScale(i, vector_zero)

				self.SecondWeapon:ManipulateBonePosition(i, vector_fuck)
				self.SecondWeapon:ManipulateBoneScale(i, vector_zero)
			end
		end

		if self.WeNeedDeployAnim then
			if not self:GetSilencer() then
				self:EasySendWeaponAnim(self.PunishedFirstDraw and "draw" or "first_draw", ACT_VM_DRAW)
			else
				self:EasySendWeaponAnim(self.PunishedFirstDraw and "draw_sil" or "first_draw_sil", ACT_VM_DRAW_SILENCED)
			end

			if self.Ignore__sv_defaultdeployspeed then
				vm:SetPlaybackRate(self.DeployPlaybackRate or 1.4)
			else
				vm:SetPlaybackRate(GetConVar("sv_defaultdeployspeed"):GetFloat())
			end

			self.WeNeedDeployAnim = false
		end
	end

	self.ViewModelFOV = GetConVar("ug_vmfov"):GetFloat()
end

local sc = Vector(1, -1, 1)
local vector_zero_, vec_default = Vector(), Vector(1, 1, 1)

function SWEP:PostDrawViewModel()
    local ply = self:GetOwner()

	if not ply:IsValid() then
		return
	end

	local vm = ply:GetViewModel()

	if not vm:IsValid() then
		return
	end

	local materials = vm:GetMaterials()

	for index = 1, #materials do
		vm:SetSubMaterial(index, "")
	end

	if self:GetDouble() and IsValid(self.SecondWeapon) then
		local oldPos, oldAng = vm:GetPos(), vm:GetAngles()

		self.SecondWeapon:SetRenderOrigin(oldPos)
		self.SecondWeapon:SetRenderAngles(oldAng)

		local mat = Matrix()
		mat:Scale(sc)

		self.SecondWeapon:EnableMatrix("RenderMultiply", mat)

		self.SecondWeapon:FrameAdvance()

		render.CullMode(1)
			self.SecondWeapon:DrawModel()
		render.CullMode(0)
	end

	for i = 1, vm:GetBoneCount() - 1 do
		local name = string.lower(vm:GetBoneName(i))

		if string.StartWith(name, "l_")
			or string.StartWith(name, "left")
			or find_whole_word(name, "l") then
			vm:ManipulateBonePosition(i, vector_zero_)
			vm:ManipulateBoneScale(i, vec_default)

			if IsValid(self.SecondWeapon) then
				self.SecondWeapon:ManipulateBonePosition(i, vector_zero_)
				self.SecondWeapon:ManipulateBoneScale(i, vec_default)
			end
		end
	end

	render.SetBlend(1)
end

function SWEP:CalcView(ply, pos, ang, fov)
    local ply = self:GetOwner()

	if not ply:IsValid() then
		return
	end

	local vm = ply:GetViewModel()

	if not vm:IsValid() then
		return
	end

	local FT = FrameTime()
	local plyVel = ply:GetVelocity()
    local vel, max = plyVel:Length(), ply:GetRunSpeed()
    local maxwalk, CT, getzoom = ply:GetWalkSpeed(), CurTime(), self:GetZoom()

	local isrunning = self:GetRunning()

    if ply:OnGround() then
        if isrunning then
            if erp_to_default[1] then
                erp_to_default, time = {}, 0
            end

            time = CT * (3.7 + math.Clamp(max / 150, 0, 3)) * 2
            erp = math.cos(time) * 1
        elseif vel > 0 and (nextthink + FT * 5) < CT then
            if erp_to_default[1] then
                erp_to_default, time = {}, 0
            end

            time = CT * (2.75 + math.Clamp(maxwalk / 120, 0, 5)) * 2
            erp = math.cos(time) * .25
        else
            if not erp_to_default[1] then
                erp_to_default = {erp, CT + 0.33}
            end

            erp = erp_to_default[1] * math.Clamp((erp_to_default[2] - CT) * 3, 0, 1)
        end
    else
		if ply:WaterLevel() < 2 then
			if not erp_to_default[1] then
				erp_to_default = {erp, CT + 0.1}
			end

			erp = erp_to_default[1] * math.Clamp((erp_to_default[2] - CT) * 2, 0, 1)
			nextthink = CT
		end
    end

	local nextprimaryfire = self:GetNextPrimaryFire()
	local isreloading, getlastshoot = self:GetReloading(), self:GetLastShoot()

	local walkRemapped = math.Clamp(math.Remap(vel, 0, maxwalk, 0, 1), 0, 1)

    lerp = Lerp(FT * (self:GetDouble() and 9 or 12), lerp, (erp * walkRemapped * self.LerpedZooming) * 1.1)
	TEST = Lerp(FT * 4, TEST, lerp)

	local normalized = plyVel:GetNormalized()

	normal = self:LerpVector(FT * 8, normal, -(normalized / 3) * walkRemapped)

	local preattack = self:GetPreAttackReady()
	local should_use_runironsights = isrunning and nextprimaryfire < CT and not isreloading and self:GetPreAttack() < CT and not preattack and not self.DisableRunningAnimation and not self:GetDouble()

	local safe = self.Firemodes[self:GetFiremode()] == "safe" and not isreloading
	local FTmult = isrunning and not preattack and not double and self.LerpedRun * .7 or 12

	if self.RunIronSightsPos then
		self.LerpedRunIronSightsPos = self:LerpVector(FT * FTmult, self.LerpedRunIronSightsPos, (safe or should_use_runironsights) and self.RunIronSightsPos or vector_zero)
	end

	if self.RunIronSightsAng then
		self.LerpedRunIronSightsAng = self:LerpAngle(FT * FTmult, self.LerpedRunIronSightsAng, (safe or should_use_runironsights) and self.RunIronSightsAng or angle_zero)
	end

	local should_use_zoomironsights = self:GetZoom()

	if self.ZoomIronSightsPos then
		self.LerpedZoomIronSightsPos = self:LerpVector(FT * 14, self.LerpedZoomIronSightsPos, should_use_zoomironsights and self.ZoomIronSightsPos or vector_zero)
	end

	if self.ZoomIronSightsAng then
		self.LerpedZoomIronSightsAng = self:LerpAngle(FT * 14, self.LerpedZoomIronSightsAng, should_use_zoomironsights and self.ZoomIronSightsAng or angle_zero)
	end

	if self.AlternativePos then
		if should_use_zoomironsights or safe or should_use_runironsights then
			self.LerpedAlternativePos = self:LerpVector(FT * 8, self.LerpedAlternativePos, vector_zero)
		else
			self.LerpedAlternativePos = self:LerpVector(FT * 8, self.LerpedAlternativePos, self.AlternativePos)
		end
	end

	if self.AlternativePos then
		if should_use_zoomironsights or safe or should_use_runironsights then
			self.LerpedAlternativeAng = self:LerpAngle(FT * 8, self.LerpedAlternativeAng, angle_zero)
		else
			self.LerpedAlternativeAng = self:LerpAngle(FT * 8, self.LerpedAlternativeAng, self.AlternativeAng)
		end
	end

	if isrunning and not self:GetPreAttackReady() and not self.DisableRunningAnimation and not self:GetDouble() then
		self.LerpedRun = Lerp(FT * 8, self.LerpedRun, 10)
	else
		self.LerpedRun = Lerp(FT * 8, self.LerpedRun, 3)
	end

	self.LerpedShoot = Lerp(FT * 24, self.LerpedShoot, 0)

	local attach = vm:GetAttachment(self.AttachmentForReloadViewBob or 1)

	if not ply:ShouldDrawLocalPlayer() then
		local val = (self.UseScope and self:GetZoom() and 5 or 8)

		if attach and ((self.EnableViewBobOnReloading and isreloading) or (self.EnableViewBobOnDeploy and nextprimaryfire > CT and not self.EnableCustomDeploy)) then
			if not (self.DisableViewBobInZoom and self:GetZoom()) then
				local mult = (self:GetDouble() and (self:GetDoubleReloadWait() and -0.5 or 0.5) or 1)

				LerpedCalcViewT[1] = Lerp(FT * val, LerpedCalcViewT[1], -(ang.p - attach.Ang.p) * .05 * (self.ReloadViewBobMultiplier or 1) * -math.abs(mult))
				LerpedCalcViewT[2] = Lerp(FT * val, LerpedCalcViewT[2], -(ang.r - attach.Ang.r) * .05 * (self.ReloadViewBobMultiplier or 1) * mult)
			end
		else
			LerpedCalcViewT[1] = Lerp(FT * val, LerpedCalcViewT[1], 0)
			LerpedCalcViewT[2] = Lerp(FT * val, LerpedCalcViewT[2], 0)
		end
	end

	if self.EnableViewBobOnDeploy and nextprimaryfire > CT and self.EnableCustomDeploy then
		LerpedCalcViewT[1] = Lerp(FT, LerpedCalcViewT[1], -2)
		LerpedCalcViewT[2] = Lerp(FT, LerpedCalcViewT[2], 2)
	end

	if not isrunning and not should_use_runironsights and not getzoom and not self:GetReloading() then
		self.LerpedBreath = Lerp(FT * 10, self.LerpedBreath, math.cos(CT * 2.25) * 0.04)
	else
		self.LerpedBreath = Lerp(FT * 10, self.LerpedBreath, 0)
	end

	if getlastshoot > CT then
		shake = math.Clamp((getlastshoot - CT) * self.ShakeMultiplier, 0, self.MaxShake or 9999)
	else
		shake = 0
	end

	if (should_use_runironsights or safe) and ang.x < 0 then
		lerpednec = Lerp(FT * 6, lerpednec, ang.x / 3)
	else
		lerpednec = Lerp(FT * 6, lerpednec, 0)
	end

	if (self.PushShakeOnZoom and getzoom) or not getzoom then
		lerpedright = self.LerpedCalcView / 2
	else
		lerpedright = Lerp(FT * 4, lerpedright, 0)
	end

	self.LerpedCalcView = Lerp(FT * 24, self.LerpedCalcView, shake)
	self.MeleePunch = Lerp(FT * 36, self.MeleePunch, self:GetMelee() and 1 or 0)

	fov = self.LerpedFOV

	local cvar, cvar_2 = GetConVar("ug_usecustomcameramovement"), GetConVar("ug_useshake")

	if cvar_2 and cvar_2:GetBool() then
		RotateAroundAxis(ang, ang:Forward(), math.Rand(0, self.LerpedCalcView / 5))
	end

	if cvar and cvar:GetBool() then
		RotateAroundAxis(ang, ang:Right(), lerpedright)

		RotateAroundAxis(ang, ang:Right(), LerpedCalcViewT[1])
		RotateAroundAxis(ang, ang:Forward(), LerpedCalcViewT[2] + TEST * .5)

		TEST2 = -ang:Right() * TEST * 7 - ang:Up() * math.abs(lerp) * 4
		pos = pos + ang:Forward() * shake / (self.ForceCalcViewMultiplier or 2.5) + TEST2
	else
		TEST2 = vec_zero
	end

	if self:GetDouble() and self:GetReloading() then
		self.DoubleReloading = Lerp(FT * 8, self.DoubleReloading, 65)
	else
		self.DoubleReloading = Lerp(FT * 16, self.DoubleReloading, 0)
	end

	local drunk_camera = GetConVar("ug_usedrunkcamera")

	if drunk_camera and drunk_camera:GetBool() then
		local CT = CurTime()

		RotateAroundAxis(ang, ang:Right(), math.sin(CT * 6) * 6)
		RotateAroundAxis(ang, ang:Forward(), math.cos(CT * 6) * 8)

		pos = pos + ang:Forward() * math.cos(CT * 6) + ang:Right() * math.cos(CT * 6)
	end

	if self:IsScopeReady() then
		fov = 8 + self.FOVInZoom

		pos = pos + TEST2 * 4
	end

	local view_ent = ply:GetViewEntity()

	if view_ent:IsValid() and view_ent:GetClass() == "gmod_cameraprop" then
		return
	end

	return pos, ang, fov
end

hook.Add("GetMotionBlurValues", "ug_MotionBlur", function(horizontal, vertical, forward, rotational)
	local cvar = GetConVar("ug_useblur")

	if cvar and not cvar:GetBool() then
		return
	end

	local ply, FT = LocalPlayer(), FrameTime()
	local self = ply:GetActiveWeapon()
	local issniperrifle = self.WeaponType == "sniper_rifle"

	if self:IsValid() and self.Base == "ug_base" then
		if self:GetRunning() then
			zoom = Lerp(FT * 8, zoom, 0)
			motionblur = Lerp(FT * 6, motionblur, 0.007)
		elseif self:GetZoom() and not ply:ShouldDrawLocalPlayer() and not self.UseScope then
			if self.WeaponType == "sniper_rifle" then
				zoom = Lerp(FT * 6, zoom, 0.01)
			else
				motionblur = Lerp(FT * 6, motionblur, 0.005)
			end
		else
			if motionblur > 0.0001 or zoom > 0.0001 then
				motionblur = Lerp(FT * 6, motionblur, 0)
				zoom = Lerp(FT * 6, zoom, 0)

				return zoom, 0, motionblur, 0
			end
		end

		return zoom, 0, motionblur, 0
	else
		if motionblur > 0.0001 or zoom > 0.0001 then
			motionblur = Lerp(FT * 6, motionblur, 0)
			zoom = Lerp(FT * 6, zoom, 0)

			return zoom, 0, motionblur, 0
		end
	end
end)
