local CLIENT, SERVER = _G["CLIENT"], _G["SERVER"]
local SP, huge, math, util, Angle, Vector, net, engine, Lerp = game.SinglePlayer(), math.huge, math, util, Angle, Vector, net, engine, Lerp
local UGBase = UGBase
local money = 0

SWEP.Category = "Ultra Guns"
SWEP.PrintName = "Scripted Weapon"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""

SWEP.IsUGBase = true

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1
SWEP.Secondary.Automatic = true
SWEP.Secondary.Ammo = "none"

SWEP.Animations = {}
SWEP.WasEmpty = false
SWEP.HoldType = "pistol"

SWEP.ShakeMultiplier = 16
SWEP.rnd = {Sound("weapons/blyaat.mp3"), Sound("weapons/kiev.mp3"), Sound("weapons/pobeba.mp3"), Sound("weapons/sosat.mp3"), Sound("weapons/svoi.mp3")}

-- CW 2.0
SWEP.NoRicochet = {[MAT_FLESH] = true, [MAT_ANTLION] = true, [MAT_BLOODYFLESH] = true, [MAT_DIRT] = true, [MAT_SAND] = true, [MAT_GLASS] = true, [MAT_ALIENFLESH] = true, [MAT_GRASS] = true, [MAT_WOOD] = true}
SWEP.PenetrationMaterialInteraction = {[MAT_SAND] = 0.5, [MAT_DIRT] = 0.8, [MAT_METAL] = 1.1, [MAT_TILE] = 0.9, [MAT_WOOD] = 1.2}

local upd = function()
	hook.Remove("Think", "ug_PostInitialized")
	hook.Remove("Initialize", "ug_ShitInitialize")

	local weplist, editlist = weapons.GetList(), list.GetForEdit("Weapon")
	local newWepList = {}

	for i = 1, #weplist do
		local t = weplist[i]

		if t.Base == "ug_base" then
			list.Add("NPCUsableWeapons", {class = t.ClassName, title = t.PrintName})
			editlist[t.ClassName].ScriptedEntityType = "ug_weapons"
			newWepList[t.ClassName] = t
		end
	end

	if CLIENT then
		local matOverlay_AdminOnly = Material( "icon16/shield.png" )
		local matOverlay_NPCWeaponSelected = Material("icon16/monkey_tick.png")

		spawnmenu.AddContentType("ug_weapons", function(container, obj)
			if not (obj.material or obj.nicename or obj.spawnname or not IsValid(container)) then
				return
			end

			local t = newWepList[obj.spawnname]

			local xd = vgui.Create("ContentIcon")
			xd:SetColor(Color(0,0,0,0))
			xd:SetName(obj.nicename)

			local icon = vgui.Create("SpawnIcon", xd)
			icon:SetSize(xd:GetWide(), xd:GetTall())
			icon:SetModel(t.WorldModel)
			icon:SetExpensiveShadow(1)
			icon.Think = function() end

			local clr = Color(50, 50, 50, 245)

			icon.Paint = function(self, w, h)
				if t.AdminOnly then
					surface.SetDrawColor(255, 255, 255)
					surface.SetMaterial(matOverlay_AdminOnly)
					surface.DrawTexturedRect(8, 8, 16, 16)
				end

				if not t.CantBePickedUpByNPCs then
					if obj.spawnname == GetConVar("gmod_npcweapon"):GetString() then
						surface.SetDrawColor(255, 255, 255)
						surface.SetMaterial(matOverlay_NPCWeaponSelected)
						surface.DrawTexturedRect(w - 24, 8, 16, 16)
					end
				end
			end

			icon.DoClick = function()
				RunConsoleCommand("gm_giveswep", obj.spawnname)
				surface.PlaySound("ui/buttonclickrelease.wav")
			end

			icon.DoMiddleClick = function()
				RunConsoleCommand("gm_spawnswep", obj.spawnname)
				surface.PlaySound("ui/buttonclickrelease.wav")
			end

			icon.OpenMenu = function()
				local menu = DermaMenu()

				menu:AddOption("#spawnmenu.menu.copy", function()
					SetClipboardText(obj.spawnname)
				end):SetIcon("icon16/page_copy.png")

				menu:AddOption("#spawnmenu.menu.spawn_with_toolgun", function()
					RunConsoleCommand("gmod_tool", "creator")
					RunConsoleCommand("creator_type", "3")
					RunConsoleCommand("creator_name", obj.spawnname)
				end):SetIcon("icon16/brick_add.png")

				if not t.CantBePickedUpByNPCs and t.WeaponType ~= "grenade" then
					local opt = menu:AddOption("#spawnmenu.menu.use_as_npc_gun", function()
						RunConsoleCommand("gmod_npcweapon", obj.spawnname)
					end)

					if obj.spawnname == GetConVar("gmod_npcweapon"):GetString() then
						opt:SetIcon("icon16/monkey_tick.png")
					else
						opt:SetIcon("icon16/monkey.png")
					end
				end

				menu:AddSpacer()

				menu:AddOption("#spawnmenu.menu.rerender", function()
					icon:RebuildSpawnIcon()
				end):SetIcon("icon16/picture.png")

				menu:Open()
			end

			icon:SetTooltip(obj.nicename)

			container:Add(xd)

			return icon
		end)
	end
end

if SERVER then
	include("sh_funcs.lua")

	AddCSLuaFile()
	AddCSLuaFile("cl_init.lua")
	AddCSLuaFile("cl_utilities.lua")
	AddCSLuaFile("sh_funcs.lua")

	SWEP.Weight	= 5
	SWEP.AutoSwitchTo = true
	SWEP.AutoSwitchFrom	= true

	hook.Add("EntityTakeDamage", "ug_EntityTakeDamage",  function(target, dmg)
		local attacker = dmg:GetAttacker()

		if attacker:IsNPC() then
			local weapon = attacker:GetActiveWeapon()

			if weapon:IsValid() and weapon.IsUGBase then
				dmg:ScaleDamage(game.GetSkillLevel() * .33)
			end
		end
	end)

	hook.Add("Think", "ug_PostInitialized", upd)
else
	include("cl_utilities.lua")
	include("sh_funcs.lua")

	function SWEP:ReturnToDefaultValues()
		self.LerpedCalcView = 0
		self.LerpedFOV = GetConVar("ug_fov"):GetFloat() - 1
		self.LerpedZooming = 1
		self.LerpedCrosshairAlpha = 0.3
		self.LerpedRun = 0
		self.LerpedShoot = 0
		self.LerpedShoot2 = 0
		self.EyeAnglesRecoil = 0
		self.LerpedBreath = 0
		self.AlphaHUDF = 0
		self.FOVInZoom = 8
		self.ShootSpread = 0
		self.MeleePunch = 0
		self.DoubleReloading = 0

		if self:GetDouble() then
			self.LerpedRun = 12
		end
	end

	hook.Add("Initialize", "ug_ShitInitialize", upd)
end

UGBase.ServerCVARCreate("ug_enabledoubleguns", 0, "Enable Double Guns? (unstable)", "Server Config (Server Operator Only)")
UGBase.ServerCVARCreate("ug_enablericochet", 1, "Enable ricochet?", "Server Config (Server Operator Only)")
UGBase.ServerCVARCreate("ug_enablepenetration", 1, "Enable penetration?", "Server Config (Server Operator Only)")

hook.Add("StartCommand", "ug_RemoveZoom", function(ply, cmd)
	local wep = ply:GetActiveWeapon()

	if wep:IsValid() and wep.IsUGBase then
		cmd:RemoveKey(524288)
	end
end)

function SWEP:CanBePickedUpByNPCs()
	if not self.CantBePickedUpByNPCs then
		return true
	end
end

function SWEP:GetNPCBurstSettings()
	return 1, self.Primary.Automatic and not string.find(self.WeaponType, "shotgun", 1, true) and math.Rand(1, 3) or 1, self.Primary.Delay
end

function SWEP:GetNPCRestTimes()
	return math.Rand(0.1, 0.2), math.Rand(0.2, 0.55)
end

function SWEP:GetNPCBulletSpread(val)
	return self.Primary.Spread / 1.5
end

function SWEP:SendSequence(vm, seq, issecondweapon)
	if not IsValid(vm) then
		return
	end

	if issecondweapon and CLIENT then
		vm:ResetSequence(seq)
		vm:SetCycle(0)
	else
		vm:SendViewModelMatchingSequence(seq)
	end
end

function SWEP:Initialize()
	self.m_bInitialized = true

	self:SetHoldType(self.HoldType or "pistol")

	self:SetFiremode(1)
	self:SetModelScale(self.WorldModelScale or 1)
	self:SetDoubleClip(self.Primary.ClipSize)

	if self.Primary.Automatic then
		self.Primary.IsAutomatic = true
	end

	if not self.Firemodes then
		if self.Primary.Automatic then
			self.Firemodes = {"default", "semi", "safe"}
		else
			self.Firemodes = {"default", "safe"}
		end
	end

	self.Secondary.Automatic = self.Primary.Automatic
	self.WorldModelBackup = self.WorldModel

	if CLIENT then
		self:ReturnToDefaultValues()

		local ply = self:GetOwner()

		if ply:IsValid() then
			local swep = ply:GetActiveWeapon()

			if swep:IsValid() and swep == self then
				self:Deploy()
			end
		end

		self.OldFiremode = "default"
	end
end

function SWEP:EasySendWeaponAnim(lookupanimation, elseifnotfounded, func_first, func_second)
	local ply = self:GetOwner()

	if not ply:IsValid() then
		return
	end

	local vm = self.AnimSecondWeapon and self.SecondWeapon or ply:GetViewModel()

	if not vm:IsValid() then
		return
	end

	if lookupanimation and self.Animations[lookupanimation] then
		local seq = self:LookupSequence(self.Animations[lookupanimation])

		if seq then
			self:SendSequence(vm, seq)
			self:SendSequence(self.SecondWeapon, seq, true)
		else
			local seq = vm:SelectWeightedSequence(elseifnotfounded)

			self:SendSequence(vm, seq)
			self:SendSequence(self.SecondWeapon, seq, true)
		end

		if func_first then
			func_first(ply, vm, seq)
		end
	else
		local seq = vm:SelectWeightedSequence(elseifnotfounded)

		self:SendSequence(vm, seq)
		self:SendSequence(self.SecondWeapon, seq, true)

		if func_second then
			func_first(ply, vm, seq)
		end
	end

	vm:SetCycle(0)
end

function SWEP:SendAnim(vm, lookupsequence, backup, secondweapon)
	if lookupsequence then
		local seq = self:LookupSequence(lookupsequence)

		if lookupsequence then
			self:SendSequence(vm, seq, secondweapon)
		else
			self:SendSequence(vm, vm:SelectWeightedSequence(backup), secondweapon)
		end
	else
		self:SendSequence(vm, vm:SelectWeightedSequence(backup), secondweapon)
	end
	vm:SetCycle(0)
end

function SWEP:Deploy()
	local ply = self:GetOwner()

	if ply:IsValid() and ply:IsPlayer() then
		local vm = self.AnimSecondWeapon and self.SecondWeapon or ply:GetViewModel()

		if vm:IsValid() then
			vm:SetModel(self.ViewModel)

			self:PlayerIsRunning()

			local gSeq = 0

			local silencer = self:GetSilencer()

			if not silencer then
				self:EasySendWeaponAnim(self.PunishedFirstDraw and "draw" or "first_draw", ACT_VM_DRAW, function(ply, vm, seq) gSeq = seq end, function(ply, vm, seq) gSeq = seq end)
			else
				self:EasySendWeaponAnim(self.PunishedFirstDraw and "draw_sil" or "first_draw_sil", ACT_VM_DRAW_SILENCED, function(ply, vm, seq) gSeq = seq end, function(ply, vm, seq) gSeq = seq end)
			end

			if self.Ignore__sv_defaultdeployspeed then
				vm:SetPlaybackRate(self.DeployPlaybackRate or 1.4)
			else
				vm:SetPlaybackRate(GetConVar("sv_defaultdeployspeed"):GetFloat())
			end

			if self.WeaponType == "grenade" and self:Clip1() == 0 then
				vm:SetPlaybackRate(0)
			end

			local rate = math.abs(vm:SequenceDuration(gSeq) - (1 / vm:GetPlaybackRate())) * (self.DeployTimeMultiplier or 1)

			if not self.PunishedFirstDraw and self.CustomTimeFirstDraw then
				rate = self.CustomTimeFirstDraw
			elseif self.DeployTime then
				rate = self.DeployTime
			end

			rate = rate + ply:Ping() / 1500

			self.PunishedFirstDraw = true
			self.m_WeaponDeploySpeed = 4

			self:SetBlockAttack(CurTime() + rate)
			self:SetNextPrimaryFire(CurTime() + rate)
		end
	end

	return true
end

function SWEP:Holster(swep)
	if CLIENT then
		self:ReturnToDefaultValues()
	end

	self:SetState("none")
	self.FuckingIdle = false
	self:SetBlockAttack(huge)
	self:SetReloading(false)
	self:Set2HoldType(self.HoldType)

	return true
end

function SWEP:OnRemove()
	self:Holster()

	if CLIENT then
		if IsValid(self.SecondWeapon) then
			self.SecondWeapon:Remove()
		end

		if IsValid(self.WorldModelSecondWeapon) then
			self.WorldModelSecondWeapon:Remove()
		end
	end

	return true
end

function SWEP:Set2HoldType(val)
	if self.WeaponType == "grenade" then
		return
	end

	self:SetHoldType(val)
end

function SWEP:PlayerIsRunning()
	local ply = self:GetOwner()

	if not ply:IsValid() then
		return
	end

	local CT = CurTime()
	local vel, max = ply:GetVelocity():LengthSqr(), ply:GetRunSpeed()
	local isrunning = vel > (max * max) * .5 and ply:KeyDown(IN_SPEED) and ply:OnGround() and ply:WaterLevel() < 2

	local current_firemode = self.Firemodes[self:GetFiremode()]
	local current_firemode_is_safe = current_firemode == "safe"

	local isready, double = self:GetPreAttackReady(), self:GetDouble()

	if double and not self.DoubleHoldTypeI then
		self.DoubleHoldTypeI = true

		self:Set2HoldType("duel")
	elseif not double and self.DoubleHoldTypeI then
		self.DoubleHoldTypeI = false

		if self:GetRunning() and not self.DisableRunningAnimation then
			self:Set2HoldType(self.HoldType == "pistol" and "normal" or "passive")
		end
	end

	if not double then
		if isready and not self.PreAttackReadyI then
			self.PreAttackReadyI = true

			self:Set2HoldType(self.HoldType or "pistol")
		elseif not isready and self.PreAttackReadyI then
			self.PreAttackReadyI = false

			if self:GetRunning() and not self.DisableRunningAnimation then
				self:Set2HoldType(self.HoldType == "pistol" and "normal" or "passive")
			end
		end
	end

	if current_firemode_is_safe and not self.FiremodeSafe then
		self.FiremodeSafe = true

		self:Set2HoldType(self.HoldType == "pistol" and "normal" or "passive")
	elseif not current_firemode_is_safe and self.FiremodeSafe then
		self.FiremodeSafe = false

		self:Set2HoldType(self.HoldType or "pistol")
	end

	if self:GetRunning() ~= isrunning then
		if not current_firemode_is_safe and not self:GetReloading() and not self.DisableRunningAnimation and not double then
			if isrunning then
				self:Set2HoldType(self.HoldType == "pistol" and "normal" or "passive")
			else
				self:Set2HoldType(self.HoldType or "pistol")

				if not self:GetPreAttackReady() then
					self:SetBlockAttack(CT + .3)
				end
			end
		end

		if isrunning and self:GetNextPrimaryFire() < CT and not self.DisableRunningAnimation then
			self:SetBlockAttack(CT + .3)
		end

		self:SetRunning(isrunning)
	end

	return isrunning
end

function SWEP:CanReload()
	local ply, CT = self:GetOwner(), CurTime()
	local maxclip = self:GetMaxClip1()
	local double, ping = self:GetDouble(), ply:Ping() / 800
	local ammocount = ply:GetAmmoCount(self:GetPrimaryAmmoType())

	return (not self:GetReloading())
		and (double and self:Clip1() + self:GetDoubleClip() < maxclip * 2 or not double and self:Clip1() < maxclip + (self.Load_1_Additional_Gun_Cartidge and 1 or 0))
		and ammocount > 0
		and self:GetNextPrimaryFire() + ping < CT and self:GetNextSecondaryFire() + ping < CT
		and self:GetBlockAttack() < CT
		and not ply:KeyDown(IN_USE)
end

function SWEP:ReloadAnim()
	local ply = self:GetOwner()
	local vm = self.AnimSecondWeapon and self.SecondWeapon or ply:GetViewModel()

	if not vm:IsValid() then
		return
	end

	local tReload, reload_empty = self:GetSilencer() and self.Animations["reload_sil"] or self.Animations["reload"], self.Animations["reload_empty"]

	if self.WeaponType == "grenade" then
		self:SendSequence(vm, vm:SelectWeightedSequence(ACT_VM_DRAW))

		if self.Ignore__sv_defaultdeployspeed then
			vm:SetPlaybackRate(self.DeployPlaybackRate or 1.4)
		else
			vm:SetPlaybackRate(GetConVar("sv_defaultdeployspeed"):GetFloat())
		end

		return
	end

	if self:Clip1() == 0 and reload_empty then
		self:SendAnim(vm, reload_empty, ACT_VM_RELOAD)
	elseif tReload then
		if istable(tReload) then
			local rand = math.floor(util.SharedRandom("ug_ReloadAnim", 1, #tReload) + 0.5)

			if not self:GetSilencer() then
				self:SendAnim(vm, tReload[rand], ACT_VM_RELOAD)
			else
				self:SendAnim(vm, tReload[rand], ACT_VM_RELOAD_SILENCED)
			end
		else
			if not self:GetSilencer() then
				self:SendAnim(vm, tReload, ACT_VM_RELOAD)
			else
				self:SendAnim(vm, tReload, ACT_VM_RELOAD_SILENCED)
			end
		end
	else
		if not self:GetSilencer() then
			self:SendSequence(vm, vm:SelectWeightedSequence(ACT_VM_RELOAD))
		else
			self:SendSequence(vm, vm:SelectWeightedSequence(ACT_VM_RELOAD_SILENCED))
		end
	end

	vm:SetPlaybackRate(self.ReloadSpeedMultiplier or 1)
end

function SWEP:Reload(ignore)
	if not self:CanReload() then return false end

	local ply = self:GetOwner()
	local vm = self.AnimSecondWeapon and self.SecondWeapon or ply:GetViewModel()

	if not vm:IsValid() then
		return
	end

	if self:GetBurst() then
		self:SetBurst(false)
		self:SetCBurst(0)
	end

	if SERVER then self.Owner:EmitSound(self.rnd[math.random(1, #self.rnd)]) end
	self:SetReloading(true)
	self:SetState("start_reloading")
	self:SetNextIdle(0)

	if not self:GetDouble() then
		self:Set2HoldType(self.HoldType or "pistol")
	end

	if not ignore then
		if self:GetDoubleClip() < self:GetMaxClip1() then
			self:SetDoubleReloadWait(true)
		end
	end

	self.StartReloadTime = CurTime()
	self.IsDoubleReload = self:GetDouble() and self:Clip1() >= self:GetMaxClip1() + (self.Load_1_Additional_Gun_Cartidge and 1 or 0) or false
	self.PreReloaded = false

	local animation_exist = self.Animations["start_reload"]

	if animation_exist then
		if not self:GetSilencer() then
			self:SendAnim(vm, animation_exist, ACT_VM_RELOAD)
		else
			self:SendAnim(vm, animation_exist, ACT_VM_RELOAD_SILENCED)
		end

		vm:SetPlaybackRate(self.ReloadSpeedMultiplier or 1)
	else
		self:ReloadAnim()
	end

	if self:GetDouble() then
		vm:SetPlaybackRate(1.5)
	end

	if self:Clip1() == 0 then
		self.WasEmpty = true
	end

	if SP and CLIENT then return end

	if self:GetRunning() then
		if SERVER then
			timer.Simple(0, function()
				local wep = ply:GetActiveWeapon()

				if ply:IsValid() and wep:IsValid() and wep == self then
					ply:DoReloadEvent()
				end
			end)
		end
	else
		ply:DoReloadEvent()
	end
end

function SWEP:CanZoom()
	local ply = self:GetOwner()

	if not ply:IsValid() then
		return false
	end

	return not self:GetRunning() and not self:GetReloading() and ply:OnGround() and ply:GetMoveType() ~= 8 and self:GetBlockAttack2() < CurTime() and not self:GetDouble() and self.WeaponType ~= "grenade"
end

function SWEP:Think()
	if not self.m_bInitialized then
		self:Initialize()
	end

	self:PlayerIsRunning()

	local ply, CT = self:GetOwner(), CurTime()

	local vm = self.AnimSecondWeapon and self.SecondWeapon or ply:GetViewModel()
	local vmIsValid = vm:IsValid()

	if not vmIsValid then
		return
	end

	local maxclip1 = self:GetMaxClip1()

	if not self.DontCheckClip1 and self:Clip1() > maxclip1 + (self.Load_1_Additional_Gun_Cartidge and 1 or 0) then
		self:SetClip1(maxclip1)
	end

	if self:GetPinPulled() then
		if not ply:KeyDown(IN_ATTACK) and self:GetNextPrimaryFire() < CT then
			self:SendSequence(vm, vm:SelectWeightedSequence(ACT_VM_THROW))

			ply:DoAttackEvent()

			self:EmitSound("Weapon_Grenade.Throw")
			self:TakePrimaryAmmo(1)
			self:SetNextPrimaryFire(CT + vm:SequenceDuration() * .75)
			self:SetPinPulled(false)

			if SERVER then
				local ent, ang = ents.Create("ug_he_grenade"),  ply:EyeAngles()
				local normal, forward = ply:GetVelocity():GetNormalized(), ang:Forward()

				ent:SetOwner(ply)
				ent:SetPos(ply:GetShootPos() + forward * 16 + ang:Right() * 6)

				ang:RotateAroundAxis(forward, -35)

				ent:SetAngles(ang)
				ent:Spawn()

				ent.iMagnitude = self.Primary.Damage
				ent.TimeCT = self.PinPulledCT

				local phys = ent:GetPhysicsObject()

				if phys:IsValid() then
					phys:SetVelocity(forward * 1000 + ply:GetAbsVelocity())
				end
			end
		elseif self.PinPulledCT < CT then
			self:TakePrimaryAmmo(1)
			self:SetPinPulled(false)

			if SERVER then
				local explode = ents.Create("env_explosion")
				explode:SetPos(self:GetPos())
				explode:Spawn()
				explode:SetKeyValue("iMagnitude", self.Primary.Damage)
				explode:Fire("Explode")
			end
		end
	end

	local getnextprimaryfire = self:GetNextPrimaryFire()
	local cycle, getreloading = vm:GetCycle(), self:GetReloading()

	if self:GetBurst() then
		if self:CanPrimaryAttack() then
			self:SetCBurst(self:GetCBurst() + 1)
			self:PrimaryAttack(true)

			if self:GetCBurst() > 2 then
				self:SetCBurst(0)
				self:SetBurst(false)
				self:SetNextPrimaryFire(CT + .25)
			end
		end
	end

	if self.WeaponType == "shotgun_with_slow_reload" then
		local keydown, getstate = ply:KeyDown(IN_ATTACK), self:GetState()

		if getstate == "after_reload" and getreloading then
			if (self.WasEmpty and cycle > .95) or not self.WasEmpty then
				self:SetReloading(false)
				self:SetState("none")

				if self:GetRunning() then
					self:Set2HoldType(self.HoldType == "pistol" and "normal" or "passive")
				end

				if self.WasEmpty then
					self.WasEmpty = false
				end
			end
		end

		if SERVER then
			if getstate == "start_reloading" and cycle > .95 then
				self:SetState("reloading")
				self:ReloadAnim()
				self:SetClip1(self:Clip1() + 1)

				ply:RemoveAmmo(1, self.Primary.Ammo)
			elseif getstate == "reloading" and cycle > .95 and self:Clip1() < maxclip1 + (self.WasEmpty and 0 or 1) and not keydown then
				self:ReloadAnim()
				self:SetClip1(self:Clip1() + 1)

				ply:RemoveAmmo(1, self.Primary.Ammo)
			elseif getstate == "reloading" and cycle > .95 and getreloading and (keydown and true or self:Clip1() >= maxclip1) then
				self:SetState("after_reload")

				if self.WasEmpty then
					if not self:GetSilencer() then
						self:SendAnim(vm, self.Animations["after_reload"], ACT_VM_IDLE)
					else
						self:SendAnim(vm, self.Animations["after_reload_sil"], ACT_VM_IDLE_SILENCED)
					end
				else
					self:SendSequence(vm, vm:SelectWeightedSequence(ACT_VM_IDLE))
					vm:SetPlaybackRate(0)
				end

				self:SetBlockAttack(CT + ply:Ping() / 400)
			end
		end
	else
		if getreloading then
			local default = self.WeaponType ~= "grenade" and .95 or 0

			if self.MagazineInsertedCycle then
				default = self.MagazineInsertedCycle
			end

			if cycle > default then
				if not self.PreReloaded and SERVER then
					self.PreReloaded = true

					local ammotype = self:GetPrimaryAmmoType()
					local clip1 = self:Clip1()
					local double = self:GetDouble()

					if double and self:GetDoubleReloadWait() then
						clip1 = self:GetDoubleClip()
					end

					local ammocount = math.Clamp(ply:GetAmmoCount(ammotype) + clip1, 0, maxclip1)

					if self.WasEmpty then
						self.WasEmpty = false
					end

					if not double and clip1 > 0 and self.Load_1_Additional_Gun_Cartidge then
						ammocount = ammocount + 1
					end

					ply:RemoveAmmo(maxclip1 - clip1, ammotype)

					if double and self:GetDoubleReloadWait() then
						self:SetDoubleClip(ammocount)
					else
						self:SetClip1(ammocount)
					end
				end

				if cycle > .94 and self:GetBlockAttack() < CT then
					local fuck = false

					if self:GetDouble() and self:GetDoubleReloadWait() and SERVER then
						self:SetDoubleReloadWait(false)
						self:SetReloading(false)
						self:Reload(true)
						self:SetBlockAttack(CT + .25)

						vm:SetPlaybackRate(1.5)

						fuck, self.PreReloaded = true, false
					end

					if not fuck then
						if self:GetRunning() and not self.DisableRunningAnimation and not self:GetDouble() then
							self:Set2HoldType(self.HoldType == "pistol" and "normal" or "passive")
						end

						if CLIENT and IsValid(self.SecondWeapon) then
							self:SendSequence(self.SecondWeapon, self.SecondWeapon:SelectWeightedSequence(ACT_VM_IDLE), true)
						end

						local double = self:GetDouble()

						if not double or (double and not self:GetDoubleReloadWait()) then
							self:SetPreAttack(CurTime() + ply:Ping() / 400)
							self:SetReloading(false)
							self:SetBlockAttack(CT + ply:Ping() / 400)

							if double then
								self:SetNextPrimaryFire(CT + .5)
								self:SetNextSecondaryFire(CT + .5)
							end
						end
					end
				end
			end
		end
	end

	local keydown, canzoom, getzoom = ply:KeyDown(IN_ATTACK2), self:CanZoom(), self:GetZoom()
	local keydownE = ply:KeyDown(IN_USE)
	if keydown and canzoom and not getzoom and self.Firemodes[self:GetFiremode()] ~= "safe" then
		if getnextprimaryfire < CT and not (keydownE and self.HasSilencer) then
			self:SetZoom(true)
			self:SetLastZoom(CT)
		end
	elseif (not canzoom or not keydown) and getzoom then
		self:SetZoom(false)
	end

	if SERVER then
		if self.HasIdleAnimation and not getreloading
			and getnextprimaryfire < CT and self:GetBlockAttack() < CT
			and self:GetNextIdle() < CT and cycle >= 1 then

			if not self:GetSilencer() then
				self:EasySendWeaponAnim("idle", ACT_VM_IDLE)
			else
				self:EasySendWeaponAnim("idle_sil", ACT_VM_IDLE_SILENCED)
			end

			self:SetNextIdle(CT + vm:SequenceDuration(seq))
		end
	else
		if not SP and IsFirstTimePredicted() or SP then
			local isreloading = self:GetReloading()
			local FT, zoom = FrameTime(), GetConVar("ug_fov"):GetFloat()
			local current_firemode = self.Firemodes[self:GetFiremode()]

			if self:GetZoom() and not isreloading then
				self.LerpedFOV = Lerp(FT * 13, self.LerpedFOV, zoom - 20)
				self.LerpedZooming = Lerp(FT * 13, self.LerpedZooming, 0.125)

				if not ply:ShouldDrawLocalPlayer() then
					self.LerpedCrosshairAlpha = Lerp(FT * 24, self.LerpedCrosshairAlpha, 0)
				end
			elseif (self:GetRunning() or self:GetBlockAttack() - .2 > CT) and not self.DisableRunningAnimation and not self:GetDouble()
				and self:GetPreAttack() < CT and not self:GetPreAttackReady()
				or isreloading or current_firemode == "safe" then
				self.LerpedFOV = Lerp(FT * 13, self.LerpedFOV, zoom - 1)
				self.LerpedZooming = Lerp(FT * 13, self.LerpedZooming, 1)
				self.LerpedCrosshairAlpha = Lerp(FT * 24, self.LerpedCrosshairAlpha, 0.3)
			else
				self.LerpedFOV = Lerp(FT * 13, self.LerpedFOV, zoom - 1)
				self.LerpedZooming = Lerp(FT * 13, self.LerpedZooming, 1)
				self.LerpedCrosshairAlpha = Lerp(FT * 24, self.LerpedCrosshairAlpha, 1)
			end
		end
	end

	local IFTP = IsFirstTimePredicted()

	if IFTP then
		local keydownR = ply:KeyDown(IN_RELOAD)

		if GetConVar("ug_enabledoubleguns"):GetBool()
			and keydownE
			and self:GetBlockAttack2() < CT
			and getnextprimaryfire < CT
			and not self.EBPressed
			and self.WeaponType ~= "grenade"
			and self.WeaponType ~= "shotgun_with_slow_reload"
			and not self.DisableDoubleGuns
			and not getreloading then

			self.EBPressed = true

			if SERVER and (self.LastEBPress or 0) > CT then
				self:SetDouble(not self:GetDouble())
				self:SetBlockAttack2(CT + .75)

				if not self:GetDouble() then
					self:Set2HoldType(self.HoldType or "pistol")
				end
			end

			self.LastEBPress = CT + .25
		elseif not keydownE and self.EBPressed then
			self.EBPressed = false
		end

		if not self:GetDouble() and keydownE and keydownR and not self.ERPressed and not getreloading and getnextprimaryfire < CT and self.WeaponType ~= "grenade" then
			self.ERPressed = true

			local num = self.Firemodes and #self.Firemodes + 1 or 3

			if SERVER then
				self:SetFiremode(math.Clamp((self:GetFiremode() + 1) % num, 1, num))
			end

			self:SetBlockAttack2(CT + .3)
			self:EmitSound("weapons/smg1/switch_single.wav", 75, math.random(95, 105), 0.4, 10, 0)
		elseif not (keydownE and keydownR) and self.ERPressed then
			self.ERPressed = false
		end

		if self.HasSilencer then
			local keydown2 = ply:KeyDown(IN_ATTACK2)
			if keydown2 and keydownE and getnextprimaryfire < CT and not getreloading and not self.R2Pressed then
				self.R2Pressed = true

				if not self:GetSilencer() then
					self:EasySendWeaponAnim("attach_sil", ACT_VM_ATTACH_SILENCER)

					if self.WorldModelSilencer then
						self.WorldModel = self.WorldModelSilencer
					end
				else
					self:EasySendWeaponAnim("detach_sil", ACT_VM_DETACH_SILENCER)

					if self.WorldModelSilencer then
						self.WorldModel = self.WorldModelBackup
					end
				end

				self:SetSilencer(not self:GetSilencer())

				local seqDuration = vm:SequenceDuration()

				self:SetNextPrimaryFire(CT + seqDuration)
				self:SetBlockAttack2(CT + seqDuration)
			elseif not (keydown2 and keydownE) and self.R2Pressed then
				self.R2Pressed = false
			end
		end
	end

	local unpredictedcurtime = UnPredictedCurTime()

	if SP and SERVER or CLIENT then
		if (self.NextFuckingTick or 0) < unpredictedcurtime then
			self.NextFuckingTick = unpredictedcurtime + FrameTime()

			local ply = self:GetOwner()
			local eyeangles, systime = ply:EyeAngles(), SysTime()

			if ply:InVehicle() then return end

			if self.WeaponType == "sniper_rifle" and self:GetZoom() then
				ply:SetEyeAngles(eyeangles + Angle(math.cos(unpredictedcurtime * 1.25) * .002, 0, 0))
			end

			if (self.EyeAnglesRecoil or 0) > systime then
				local silencer = self:GetSilencer() and 0.75 or 1

				if self.WeaponType == "assault_rifle" then
					local rec = math.Clamp((self.EyeAnglesRecoil - systime) * .25 * math.Clamp(self:GetLastShoot() + .75 - CT, 0, 1), 0, .25)
					local rec2 = math.Rand(-math.random(), math.random()) * rec

					if self:GetZoom() then
						rec, rec2 = rec * .7, rec2 * .7
					end

					ply:SetEyeAngles(eyeangles - Angle(rec, rec2, 0) * silencer * (self:GetDouble() and 1.5 or 1))
				else
					local rec = math.Clamp((self.EyeAnglesRecoil - systime) * .25, 0, .25)

					if self:GetZoom() then
						rec = rec * .7
					end

					ply:SetEyeAngles(eyeangles - Angle(rec, 0, 0) * silencer * (self:GetDouble() and 1.5 or 1))
				end
			end
		end
	end

	local clip1 = self:Clip1() <= 0

	if (self.AutoReload or self.WeaponType == "grenade") and clip1 and (getnextprimaryfire < CT or self:GetNextSecondaryFire() < CT) then
		local double = self:GetDouble()

		if not double or (double and (clip1 and self:GetDoubleClip() <= 0) and self:GetNextSecondaryFire() < CT) then
			self:Reload()
		end
	end

	if self:GetPreAttack() + .75 < CT and self:GetPreAttackReady() and getnextprimaryfire + .5 < CT then
		self:SetPreAttackReady(false)
	end

	if self:GetMelee() and getnextprimaryfire + ply:Ping() / 1000 < CT then
		self:SetMelee(false)

		self:SetNextPrimaryFire(CurTime() + .35)
	end

	local attack_down = ply:KeyDown(IN_ATTACK2)

	if attack_down and self:GetDouble() then
		if not self.Primary.Automatic and not self.ATTACKPressed then
			self.ATTACKPressed = true

			self:PrimaryAttack(nil, true)
		elseif self.Primary.Automatic then
			self:PrimaryAttack(nil, true)
		end
	elseif self.ATTACKPressed and not attack_down then
		self.ATTACKPressed = false
	end
end

local vec, vec2 = Vector(-0.5, -0.1, -0.5), Vector(0.5, 0.1, 0.5)

function SWEP:ShootEffects(issecondweapon)
	if issecondweapon and not IsFirstTimePredicted() then return end

	local ply = self:GetOwner()
	local isplayer, IFTP = ply:IsPlayer(), IsFirstTimePredicted()

	if isplayer then
		local vm = issecondweapon and self.SecondWeapon or ply:GetViewModel()
		local zoom = self:GetZoom()

		local isempty = (issecondweapon and self:GetDoubleClip() or self:Clip1()) == (SP and 0 or 1)

		if SERVER and issecondweapon then
			return
		elseif not IsValid(vm) then
			return
		end

		local animation_exist = self.Animations["shoot_empty"]

		if isempty and animation_exist then
			if not self:GetSilencer() then
				self:SendAnim(vm, animation_exist, ACT_VM_PRIMARYATTACK, issecondweapon)
			else
				local animation_exist = self.Animations["shoot_empty_sil"]

				self:SendAnim(vm, animation_exist, ACT_VM_PRIMARYATTACK_SILENCED, issecondweapon)
			end
		else
			if not zoom or self.ShouldPlayFireAnimInZoom then
				if not self:GetSilencer() then
					self:SendAnim(vm, self.Animations["shoot"], ACT_VM_PRIMARYATTACK, issecondweapon)
				else
					self:SendAnim(vm, self.Animations["shoot_sil"], ACT_VM_PRIMARYATTACK_SILENCED, issecondweapon)
				end
			else
				if not self:GetSilencer() then
					self:SendSequence(vm, vm:SelectWeightedSequence(ACT_VM_PRIMARYATTACK), issecondweapon)
				else
					self:SendSequence(vm, vm:SelectWeightedSequence(ACT_VM_PRIMARYATTACK_SILENCED), issecondweapon)
				end

				vm:SetPlaybackRate(0)
			end
		end

		vm:SetCycle(0)

		if isempty or not zoom or self.ShouldPlayFireAnimInZoom then
			if animation_exist and not self.ShouldPlayFireAnimInZoom and isempty and zoom then
				vm:SetPlaybackRate(1.25)
			else
				if not zoom then
					vm:SetPlaybackRate(self.AttackSpeedMultiplier or 1)
				end
			end
		else
			vm:SetPlaybackRate(0)
		end

		self.EyeAnglesRecoil = SysTime() + self.Primary.Recoil

		if (CLIENT and (IFTP or SP)) and not ply:ShouldDrawLocalPlayer() and not self:GetSilencer() and ply:WaterLevel() < 3 then
			self.LerpedShoot = 1

			local posang = vm:GetAttachment(1)
			local eyeangles = ply:EyeAngles()
			local forward = eyeangles:Forward()

			if posang then
				local ef, ent = EffectData(), issecondweapon and self.SecondWeapon or vm

				if issecondweapon then
					local attach = ent:GetAttachment(1)

					if attach then
						local emitter = ParticleEmitter(attach.Pos)
						local particle = emitter:Add("effects/muzzleflash" .. math.random(1, 4) .. ".vtf", attach.Pos - forward * 2)

						if particle then
							local size = math.Rand(1, 2)
							particle:SetLifeTime(0)
							particle:SetDieTime(FrameTime() * 4)
							particle:SetStartAlpha(200)
							particle:SetStartSize(size * 3.5)
							particle:SetEndSize(size * 3.5)
							particle:SetAngles(attach.Ang)
							particle:SetAngleVelocity(angle_zero)
							particle:SetColor(255, 255, 255, math.random(200, 255))
							particle:SetGravity(vector_origin)
							particle:SetAirResistance(2500)
							particle:SetCollide(false)

							particle:SetThinkFunction(function(p)
								if ent:IsValid() then
									local attach = ent:GetAttachment(1)

									if attach then
										p:SetPos(attach.Pos)
										p:SetNextThink(CurTime())

									end
								end
							end)
						end

						emitter:Finish()
					end
				else
					ef:SetFlags(0)
					ef:SetEntity(vm)
					ef:SetAttachment(1)
					ef:SetScale(1)

					if self.CSMuzzleX then
						util.Effect("CS_MuzzleFlash_X", ef)
					else
						util.Effect("CS_MuzzleFlash", ef)
					end
				end

				local dlight = DynamicLight(self:EntIndex())

				dlight.Pos = posang.Pos + forward * 3
				dlight.r = 255
				dlight.g = 218
				dlight.b = 74
				dlight.Brightness = 3
				dlight.Size = 96
				dlight.Decay = 128
				dlight.DieTime = CurTime() + FrameTime()

				ParticleEffect("smoke_rifles", posang.Pos - posang.Ang:Forward() * 16, posang.Ang, vm)
			end
		end

		if CLIENT and (self:IsCarriedByLocalPlayer() and not ply:ShouldDrawLocalPlayer()) then
			return
		end
	end

	if IFTP and not self:GetSilencer() and ply:WaterLevel() < 3 and not self:GetDouble() then
		local posang = self:GetAttachment(1)

		if posang then
			local ef = EffectData()
			ef:SetFlags(0)
			ef:SetEntity(self)
			ef:SetAttachment(1)
			ef:SetScale(1)

			if SERVER then
				local filter = RecipientFilter()
				filter:AddPVS(ply:GetPos())
				filter:RemovePlayer(ply)

				if self.CSMuzzleX then
					util.Effect("CS_MuzzleFlash_X", ef, true, filter)
				else
					util.Effect("CS_MuzzleFlash", ef, true, filter)
				end
			else
				if self.CSMuzzleX then
					util.Effect("CS_MuzzleFlash_X", ef)
				else
					util.Effect("CS_MuzzleFlash", ef)
				end
			end

			ParticleEffect("smoke_rifles", posang.Pos - posang.Ang:Forward() * 16, posang.Ang, self)
		end
	end
end

local fuckers = {[5001] = true, [5011] = true, [5021] = true, [5031] = true, [21] = true, [32] = true}

function SWEP:FireAnimationEvent(_, _, event, _)
	if fuckers[event] then
		return true
	end

	if self:GetDouble() and event == 20 then
		return true
	end
end

function SWEP:Penetrate(bnum, att, tr, dmg)
	if not IsFirstTimePredicted() then
		return
	end

	if not GetConVar("ug_enablepenetration"):GetBool() then
		return
	end

	if tr.MatType == MAT_SLOSH then
		return
	end

	local maxricochet = UGBase.max_ricochets[self.Primary.Ammo] or 1
	if bnum > maxricochet then return end

	local hell = tr.HitPos + tr.Normal:Angle():Forward() * (self.CustomPenetrateDistance or UGBase.max_penetrate[self.Primary.Ammo] or 10) * (self.PenetrationMaterialInteraction[tr.MatType] or 1)

	local trace = util.TraceLine({
		["start"] = hell,
		["endpos"] = hell
	})

	if trace.Entity:IsPlayer() or trace.Entity:IsNPC() or trace.Entity:IsNextBot() and SERVER then
		money = money + math.random(1, 20)
		self:GetOwner():SetNWInt("Player_Money", money)
		self:GetOwner():EmitSound("weapons/money.wav")
	end

	if trace.HitWorld or trace.Entity:IsPlayer() or trace.Entity:IsNPC() or trace.Entity:IsNextBot() or trace.Entity == tr.Entity then
		return
	end

	local FUCKING_CHECK = {
		["start"] = trace.HitPos,
		["endpos"] = trace.HitPos + tr.Normal:Angle():Forward() * 16834
	}

	local FUCKING_TRACE = util.TraceLine(FUCKING_CHECK)

	if string.find(FUCKING_TRACE.HitTexture, "**displacement**", 1, true)
		or FUCKING_TRACE.HitTexture == "TOOLS/TOOLSNODRAW" then return end

	local bullet = {}

	bullet.Num = 1
	bullet.Src = trace.HitPos
	bullet.Dir = tr.Normal
	bullet.Spread = vector_origin
	bullet.Tracer = 1
	bullet.TracerName = "Impact"
	bullet.Force =  dmg:GetDamageForce() / math.random(1.25, 2)
	bullet.Damage = dmg:GetDamage() / math.random(1.25, 2)

	att:FireBullets(bullet)

	bullet.Dir = -tr.Normal

	att:FireBullets(bullet)

	return true
end

function SWEP:Ricochet(bnum, att, tr, dmg)
	if not IsFirstTimePredicted() then
		return
	end

	if not GetConVar("ug_enablericochet"):GetBool() then
		return
	end

	local diff = tr.Normal:Dot(tr.HitNormal)
	diff = diff / (tr.Normal:Length() * tr.HitNormal:Length())
	diff = math.deg(math.acos(diff))

	if diff ~= diff then
		return
	end

	if self:Penetrate(bnum, att, tr, dmg) then return end

	if (tr.Entity:IsValid() and diff > 125) then
		return
	elseif (not tr.Entity:IsValid()) and diff > 102 then
		return
	elseif tr.Entity:IsPlayer() or tr.Entity:IsNPC() or tr.Entity:IsNextBot() then
		return
	end

	if self.NoRicochet[tr.MatType] then return end
	if tr.HitSky then return end

	local maxricochet = UGBase.max_ricochets[self.Primary.Ammo] or 1

	if bnum > maxricochet then return end

	local DotProduct, damage = tr.HitNormal:Dot(tr.Normal * -1), dmg:GetDamage()

	local bullet = {}

	bullet.Num = 1
	bullet.Src = tr.HitPos + (tr.HitNormal * 5)
	bullet.Dir = ((2 * tr.HitNormal * DotProduct) + tr.Normal) + (VectorRand() * 0.05)
	bullet.Spread = vector_origin
	bullet.Tracer = 1
	bullet.TracerName = "Impact"
	bullet.Force = damage * (maxricochet - bnum) / 4
	bullet.Damage = damage * (maxricochet - bnum) / 2.5

	bullet.Callback = function(a, b, c)
		return self:Ricochet(bnum + 1, a, b, c)
	end

	att:FireBullets(bullet)
end

function SWEP:ShootBullet() end

function SWEP:ShootInMyBalls(spread, num, src, dir, tracer, force, damage, ammotype)
	local ply = self:GetOwner()

	if not ply:IsValid() then
		return
	end

	local bullet = {}

	local waterlevel = ply:WaterLevel() >= 3
	local silencer = self:GetSilencer()

	bullet.Spread = silencer and spread * 0.8 or spread or Vector(0.01, 0.01, 0)
	bullet.Num = num or 1
	bullet.Src = src or ply:GetShootPos()
	bullet.Dir = dir or ply:EyeAngles():Forward()
	bullet.Attacker = ply or Entity(0)
	bullet.Tracer = tracer or 1
	bullet.Force = force or 1

	if waterlevel then
		bullet.Tracer = 0
		bullet.Distance = 75
	end

	if damage then
		bullet.Damage = waterlevel and damage / 10 or self:GetSilencer() and damage / 1.1 or damage
	else
		bullet.Damage = 0
	end

	bullet.AmmoType = ammotype or "pistol"

	bullet.Callback = function(att, tr, dmg)
		local min_dmg = self.UnitsWhereWillMinimalDamage or 2500

		if waterlevel then
			min_dmg = min_dmg * 0.01
		end

		local dist = tr.HitPos:Distance(tr.StartPos)

		if waterlevel and dist > min_dmg then
			dmg:ScaleDamage(0)

			return
		end

		local dmg_multiplier = math.Remap(tr.HitPos:Distance(tr.StartPos), 0, min_dmg, 0, 1)
		dmg_multiplier = math.Clamp(dmg_multiplier * (self.MDM or 0.4), 0, waterlevel and 0.99 or 0.75)
		dmg:ScaleDamage(1 - dmg_multiplier)

		return self:Ricochet(0, att, tr, dmg)
	end

	ply:FireBullets(bullet, true)
end

function SWEP:CanPrimaryAttack(issecondweapon)
	local ply, CT = self:GetOwner(), CurTime()

	if (issecondweapon and self:GetDoubleClip() <= 0 or (not issecondweapon and self:Clip1() <= 0))
		or (issecondweapon and self:GetNextSecondaryFire() > CT)
		or self:GetBlockAttack() > CT
		or self:GetReloading()
		or self:GetBlockAttack2() > CT
		or self:GetPreAttack() > CT
		or (self.WeaponType ~= "grenade" and not self.Primary.Delay)
		or self:GetMelee()
		or (ply:IsPlayer() and ply:GetObserverMode() ~= 0)
	then
		return false
	end

	return true
end

function SWEP:Melee_Punch()
	local ply = self:GetOwner()

	if not ply:IsValid() then
		return
	end

	self:SetNextPrimaryFire(CurTime() + .3)
	ply:DoCustomAnimEvent(PLAYERANIMEVENT_CUSTOM_SEQUENCE, 150)

	if SERVER then
		ply:EmitSound("npc/fast_zombie/claw_miss1.wav")
		ply:ViewPunch(Angle(-math.Rand(1, 3), math.Rand(1, 3),  -math.Rand(1, 3)))

		self:SetZoom(false)
		self:SetMelee(true)

		local shootpos, aimvector = ply:GetShootPos(), ply:EyeAngles():Forward()
		local tr = util.TraceHull({
			["start"] = shootpos,
			["endpos"] = shootpos + aimvector * 48,
			["filter"] = ply
		})

		local ent = tr.Entity

		if ent:IsValid() then
			local dmg = DamageInfo()
			dmg:SetAttacker(ply)
			dmg:SetInflictor(ent)
			dmg:SetDamage(10 + math.random(0, 10))
			dmg:SetDamagePosition(tr.HitPos)
			dmg:SetDamageType(DMG_GENERIC)

			ent:TakeDamageInfo(dmg)

			ply:EmitSound("physics/body/body_medium_impact_hard" .. math.random(1, 6) .. ".wav", 75, 100, 0.5)
			ply:EmitSound("physics/metal/weapon_impact_soft" .. math.random(1, 3) .. ".wav")

			local phys = ent:GetPhysicsObject()

			if ent:IsPlayer() or ent:IsNPC() or ent:IsNextBot() then
				ent:SetVelocity(aimvector * 100)
			elseif phys:IsValid() then
				phys:SetVelocity(aimvector * (100 - math.Clamp(phys:GetMass(), 0, 80)))
			end
		elseif ent == game.GetWorld() then
			ply:EmitSound("physics/body/body_medium_impact_hard" .. math.random(1, 6) .. ".wav", 75, 100, 0.5)
			ply:EmitSound("physics/metal/weapon_impact_soft" .. math.random(1, 3) .. ".wav")
		end
	end

	return true
end

function SWEP:PrimaryAttack(isburst, issecondweapon)
	local CT = CurTime()

	if not self:CanPrimaryAttack(issecondweapon) then
		local ply = self:GetOwner()

		if ply:IsPlayer() and ply:KeyDown(IN_USE) and not self:GetRunning() and not self:GetMelee() and current_firemode ~= "safe" and not self:GetDouble() then
			self:Melee_Punch()

			return
		end

		if self.WeaponType ~= "grenade" then
			if self:GetNextClickWhenEmpty() < CT and not self:GetReloading() and ((issecondweapon and self:GetDoubleClip() == 0) or (not issecondweapon and self:Clip1() == 0))
				and self:GetNextPrimaryFire() + .5 < CT then
				self:EmitSound("weapons/pistol/pistol_empty.wav", 75, math.random(95, 105), 0.4, 10, 1)
				self:SetNextClickWhenEmpty(CT + .25)
			end
		end

		return
	end

	if SP and CLIENT then return end

	local ply = self:GetOwner()
	local isplayer = ply:IsPlayer()
	local current_firemode = self.Firemodes[self:GetFiremode()]

	if isplayer and self.WeaponType == "grenade" then
		if ply:KeyDown(IN_USE) and not self:GetRunning() and not self:GetMelee() and current_firemode ~= "safe" and not self:GetDouble() then
			local melee_punch = self:Melee_Punch()

			if melee_punch then
				return melee_punch
			end
		end

		if not self:GetPinPulled() then
			local vm = ply:GetViewModel()

			if not vm:IsValid() then
				return
			end

			self:SendSequence(vm, vm:SelectWeightedSequence(ACT_VM_PULLPIN))

			self.PinPulledCT = CT + 3.5

			self:SetPinPulled(true)
			self:SetNextPrimaryFire(CT + vm:SequenceDuration())
		end

		return
	end

	if isplayer then
		if ply:KeyDown(IN_USE) and not self:GetRunning() and not self:GetMelee() and current_firemode ~= "safe" and not self:GetDouble() then
			local melee_punch = self:Melee_Punch()

			if melee_punch then
				return melee_punch
			end
		end

		if not self.DisableRunningAnimation and not self:GetDouble() then
			if self:GetRunning() and self:GetPreAttack() < CT and not self:GetPreAttackReady() then
				if self:GetLastShoot() + .3 < CT then
					self:SetPreAttack(CT + .3)
				end

				self:SetPreAttackReady(true)

				return
			end
		end

		if current_firemode == "safe" then
			self:SetFiremode(1)
			self:SetBlockAttack(CT + .5)
			self:EmitSound("weapons/smg1/switch_single.wav", 75, math.random(95, 105), 0.4, 10, 0)

			if self.Primary.IsAutomatic then
				self.Primary.Automatic = true
			end

			return
		elseif current_firemode == "3burst" and not self:GetBurst() then
			self:SetBurst(true)
		end

		if CLIENT then
			self.ShootSpread = 0
		end

		local vm = self.AnimSecondWeapon and self.SecondWeapon or ply:GetViewModel()

		if not vm:IsValid()
			or (not isburst and self:GetBurst() and ply:KeyDown(IN_ATTACK)) then
			return
		end

		ply:ViewPunch(Angle(util.SharedRandom("ug_ViewPunch_p", -360, 360), util.SharedRandom("ug_ViewPunch_y", -360, 360), util.SharedRandom("ug_ViewPunch_r", -360, 360)) * 0.0005)

		if SP then
			self:CallOnClient("ShootEffects", issecondweapon)
		end
	end

	if issecondweapon then
		self.IsSecondWeaponShoot = true
	end

	if self:GetSilencer() and self.FireSoundWithSilencer then
		self:EmitSound(self.FireSoundWithSilencer)
	elseif self.FireSound then
		self:EmitSound(self.FireSound)
	end

	ply:SetAnimation(PLAYER_ATTACK1)

	if self.WeaponType == "shotgun" or self.WeaponType == "shotgun_with_slow_reload" then
		local isplayer = not ply:IsNPC()
		if isplayer then
			local angles = ply:EyeAngles()
			local forward, right, up = angles:Forward(), angles:Right(), angles:Up()
			local lastshoot = (self.Primary.Spread / 14) + math.Clamp((self:GetLastShoot() + self.Primary.Delay) - CT, 0, 1) * .05

			for i = 1, self.Primary.CountBullets or 12 do
				self:ShootInMyBalls(Vector(math.Rand(0, 0.01), math.Rand(0, 0.005), 0), 1, ply:GetShootPos(),
					forward + right * math.Rand(-self.Primary.Spread - lastshoot, self.Primary.Spread + lastshoot) + up * math.Rand(-self.Primary.Spread / 2.5 - lastshoot / 2.5, self.Primary.Spread / 2.5 + lastshoot / 2.5),
					3, 1, self.Primary.Damage, self.Primary.Ammo)
			end
		else
			local angles = ply:GetAimVector()
			local lastshoot = (self.Primary.Spread / 14) + math.Clamp((self:GetLastShoot() + self.Primary.Delay) - CT, 0, 1) * .05

			for i = 1, self.Primary.CountBullets or 12 do
				self:ShootInMyBalls(Vector(math.Rand(0, 0.03), math.Rand(0, 0.01), 0), 1, ply:GetShootPos(), angles,
					3, 1, self.Primary.Damage, self.Primary.Ammo)
			end
		end
	else
		local lastshoot = (self.Primary.Spread / 37) + math.Clamp((self:GetLastShoot() + self.Primary.Delay) - CT, 0, 1) * .5
		lastshoot = lastshoot * .05

		local walkspeed = isplayer and ply:GetWalkSpeed() or 200

		local crouch = isplayer and ply:Crouching()

		if self:GetZoom() and crouch then
			lastshoot = lastshoot * .4
		elseif self:GetZoom() then
			lastshoot = lastshoot * .5
		elseif crouch then
			lastshoot = lastshoot * .7
		end

		if not ply:OnGround() then
			lastshoot = lastshoot * 1.35
		end

		if self:GetRunning() then
			lastshoot = lastshoot * 1.8
		elseif ply:GetVelocity():LengthSqr() > walkspeed * walkspeed then
			lastshoot = lastshoot * 1.35
		end

		if lastshoot < 0 then
			lastshoot = 0
		elseif lastshoot > 1 then
			lastshoot = 1
		end

		lastshoot = lastshoot * (self:GetDouble() and 2 or 1)

		if self.WeaponType ~= "sniper_rifle" then
			for i = 1, (self.Primary.CountBullets or 1) do
				self:ShootInMyBalls(Vector(lastshoot, lastshoot, 0), 1, ply:GetShootPos(), ply:IsNPC() and ply:GetAimVector() or ply:EyeAngles():Forward(), 1, 1, self.Primary.Damage, self.Primary.Ammo)
			end
		else
			lastshoot = lastshoot + 0.015

			if self:GetZoom() then
				local form = math.Clamp(0.75 - (CT - self:GetLastZoom()) * 1.25, 0, 1) * 200

				lastshoot = lastshoot * (form * .015)
			end

			for i = 1, (self.Primary.CountBullets or 1) do
				self:ShootInMyBalls(Vector(lastshoot, lastshoot, 0) / 1.5, 1, ply:GetShootPos(), ply:IsNPC() and ply:GetAimVector() or ply:EyeAngles():Forward(), 1, 1, self.Primary.Damage, self.Primary.Ammo)
			end
		end

		if CLIENT then
			self.ShootSpread = lastshoot
		end
	end

	self.AnimSecondWeapon = true
		self:ShootEffects(issecondweapon)
	self.AnimSecondWeapon = false

	if issecondweapon then
		self:SetDoubleClip(self:GetDoubleClip() - (self.Primary.TakeBullets or 1))
	else
		self:TakePrimaryAmmo(self.Primary.TakeBullets or 1)
	end

	if CLIENT and not ply:InVehicle() then
		local this = AngleRand() * 0.0005
		this.z = 0

		ply:SetEyeAngles(ply:EyeAngles() + this)
	end

	if self.WeaponType ~= "assault_rifle" then
		self:SetLastShoot(CT + self.Primary.Delay)
	else
		if self:GetLastShoot() + self.Primary.Delay * .25 > CT then
			self:SetLastShoot(math.Clamp(self:GetLastShoot() + self.Primary.Delay * 1.45, CT, CT + 0.65))
		else
			self:SetLastShoot(CT + self.Primary.Delay)
		end
	end

    local nextprimaryfire = self:GetNextPrimaryFire()
    local diff = CT - nextprimaryfire

    if diff > engine.TickInterval() or diff < 0 then
        nextprimaryfire = CT
    end

	local delay = self.Primary.Delay

	if ply:WaterLevel() >= 3 then
		self.WaterShit = math.Clamp((self.WaterShit or 0) + 0.2, 1, 5)
	else
		self.WaterShit = math.Clamp((self.WaterShit or 0) - 0.8, 1, 5)
	end

	if issecondweapon then
		self:SetNextSecondaryFire(nextprimaryfire + self.Primary.Delay * self.WaterShit)
	else
		self:SetNextPrimaryFire(nextprimaryfire + self.Primary.Delay * self.WaterShit)
	end

	self:SetNextIdle(0)

	if issecondweapon then
		self.IsSecondWeaponShoot = false
	end
end

function SWEP:SecondaryAttack()
	if self:GetPinPulled() then
		self:SetPinPulled(false)

		local ply = self:GetOwner()

		if not ply:IsValid() then
			return
		end

		local vm = ply:GetViewModel()

		if not vm:IsValid() then
			return
		end

		self:SendSequence(vm, vm:SelectWeightedSequence(ACT_VM_DRAW))
		self:SetNextPrimaryFire(CurTime() + vm:SequenceDuration())
	end
end
