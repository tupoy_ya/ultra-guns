hook.Add("AddToolMenuCategories", "ug_Utilities", function()
	spawnmenu.AddToolCategory("Utilities", "Ultra Guns", "Ultra Guns")
end)

local t_funcs = {
	["Crosshair parts"] = function(panel)
		panel:Help("Outline crosshair color:")

		local dpanel = panel:Add("DPanel")
		dpanel:SetTall(150)
		panel:AddItem(dpanel)
		dpanel.Paint = function() end

		local mix = dpanel:Add("DColorMixer")
		mix:SetPalette(true)
		mix:SetAlphaBar(false)
		mix:SetWangs(true)
		mix:SetColor(Color(255, 255, 255))
		mix:SetTall(150)

		mix.ValueChanged = function(self, color)
			local concat = table.concat({color.r, color.g, color.b}, " ")

			GetConVar("ug_outlinecrosshair"):SetString(concat)
		end

		panel:Help("Inline crosshair color:")

		local dpanel = panel:Add("DPanel")
		dpanel:SetTall(150)
		panel:AddItem(dpanel)
		dpanel.Paint = function() end

		local mix = dpanel:Add("DColorMixer")
		mix:SetPalette(true)
		mix:SetAlphaBar(false)
		mix:SetWangs(true)
		mix:SetColor(Color(255, 255, 255))
		mix:SetTall(150)

		mix.ValueChanged = function(self, color)
			local concat = table.concat({color.r, color.g, color.b}, " ")

			GetConVar("ug_inlinecrosshair"):SetString(concat)
		end
	end
}

hook.Add("PopulateToolMenu","ug_Utilities", function()
	spawnmenu.AddToolMenuOption("Utilities", "Ultra Guns", "Ultra Guns", "Settings", "", "", function(panel)
		panel:ClearControls()

		local cvars_default_client, cvars_default_server = {}, {}

		for category, t in pairs(UGBase.cvars_desc) do
			for index, var in ipairs(t) do
				local cvar, default_value = var[1], var["default_value"]

				if var["isCLIENT"] then
					cvars_default_client[cvar] = tostring(default_value)
				else
					cvars_default_server[cvar] = tostring(default_value)
				end
			end
		end

		panel:AddControl("ComboBox",
			{
				MenuButton = 1,
				Folder = "ug_default_settings",
				Options = {["#preset.default"] = cvars_default_client},
				CVars = table.GetKeys(cvars_default_client)
			}
		)

		local init_category = {}

		for category, t in SortedPairs(UGBase.cvars_desc) do
			for index, var in ipairs(t) do
				local description, cvar = var[2], var[1]

				if not init_category[category or ""] then
					init_category[category or ""] = true

					panel:Help(category or "")

					if category == "Server Config (Server Operator Only)" then
						panel:AddControl("ComboBox",
							{
								MenuButton = 1,
								Folder = "ug_default_settings_server",
								Options = {["#preset.default"] = cvars_default_server},
								CVars = table.GetKeys(cvars_default_server)
							}
						)
					end

				end

				if var[3] and var[3] == "slider" then
					panel:NumSlider(description, cvar, var[4], var[5])
				else
					panel:CheckBox(description, cvar)
				end
			end

			if t_funcs[category] then
				t_funcs[category](panel)
			end
		end
	end)
end)
