local CLIENT = _G["CLIENT"]

function SWEP:SetupDataTables()
	self:NetworkVar("String", 0, "State")

	self:NetworkVar("Int", 0, "Firemode")

	self:NetworkVar("Float", 0, "LastShoot")
	self:NetworkVar("Float", 1, "BlockAttack")
	self:NetworkVar("Float", 2, "NextIdle")
	self:NetworkVar("Float", 3, "NextClickWhenEmpty")
	self:NetworkVar("Float", 5, "BlockAttack2")
	self:NetworkVar("Float", 6, "LastZoom")
	self:NetworkVar("Float", 7, "CBurst")
	self:NetworkVar("Float", 8, "PreAttack")

	self:NetworkVar("Float", 9, "DoubleClip")

	self:NetworkVar("Bool", 0, "Running")
	self:NetworkVar("Bool", 1, "Reloading")
	self:NetworkVar("Bool", 2, "Zoom")
	self:NetworkVar("Bool", 3, "Burst")
	self:NetworkVar("Bool", 4, "PreAttackReady")
	self:NetworkVar("Bool", 5, "Melee")
	self:NetworkVar("Bool", 6, "Silencer")

	self:NetworkVar("Bool", 7, "Double")
	self:NetworkVar("Bool", 8, "DoubleReloadWait")

	self:NetworkVarNotify("Double", self.OnDoubleStatusChanged)


	if self.WeaponType == "grenade" then
		self:NetworkVar("Bool", 9, "PinPulled")
	else
		self.GetPinPulled = function()
			return false
		end
	end

	self:NetworkVarNotify("Firemode", self.OnFiremodeChanged)
end

function SWEP:OnFiremodeChanged(_, _, num)
	if self.Firemodes then
		local current_firemode = self.Firemodes[num] 

		if self:GetZoom() and current_firemode == "safe" then
			self:SetZoom(false)
		end

		if self.Primary.IsAutomatic then
			if current_firemode == "default" then
				self.Primary.Automatic = true
			elseif current_firemode == "semi" or current_firemode == "3burst" then
				self.Primary.Automatic = false
			end
		end
	end
end

function SWEP:OnDoubleStatusChanged(_, _, bool)
	if CLIENT then
		if not IsValid(self.SecondWeapon) then
			local ply = self:GetOwner()

			if not ply:IsValid() or not ply:IsPlayer() then
				return
			end

			local vm = ply:GetViewModel()

			if not vm:IsValid() then
				return
			end

			self.SecondWeapon = ClientsideModel(self.ViewModel, RENDERGROUP_VIEWMODEL)

			self.SecondWeapon:SetParent(vm)
			self.SecondWeapon:SetNoDraw(true)
			self.SecondWeapon.IsSecondWeapon = true
		end

		self.DoubleReloading = 150
	end

	self.Secondary.Ammo = bool and self.Primary.Ammo or "none"
	self:Deploy()
end

function SWEP:OnReloaded()
	self:SetupDataTables()
end

hook.Add("DoAnimationEvent", "ug_Anim", function(ply, event, data)
	if event == PLAYERANIMEVENT_CUSTOM_SEQUENCE then 
		if data == 150 then
			ply:AnimRestartGesture(GESTURE_SLOT_ATTACK_AND_RELOAD, ACT_HL2MP_GESTURE_RANGE_ATTACK_MELEE2, true)

			return ACT_INVALID
		end
	end
end)
