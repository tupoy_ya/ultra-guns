-- https://gamebanana.com/mods/210719

SWEP.Base = "ug_base"
SWEP.Category = "Ultra Guns"
SWEP.PrintName = "R700"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""
SWEP.Slot = 2

SWEP.ViewModelFOV	= 68
SWEP.ViewModelFlip	= false

SWEP.ViewModel		= "models/weapons/v_snip_k98_ug.mdl"
SWEP.WorldModel		= "models/weapons/w_snip_k98_ug.mdl"

SWEP.Spawnable		= true
SWEP.AdminOnly		= false

SWEP.Primary.ClipSize		= 7
SWEP.Primary.DefaultClip	= 9999
SWEP.Primary.TakeBullets		= 1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "7.62x51MM"
SWEP.Primary.Delay  		= 60 / 45
SWEP.Primary.Spread = 16
SWEP.Primary.Damage = 97
SWEP.Primary.Recoil = .4

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.CSMuzzleX = false
SWEP.CSMuzzleFlashes = false

SWEP.WeaponType = "sniper_rifle"
SWEP.HoldType = "ar2"

SWEP.Ignore__sv_defaultdeployspeed = true
SWEP.HasIdleAnimation = true
SWEP.ShakeMultiplier = 2.15
SWEP.MouseSensivityInZoom = 0.1
SWEP.FireSound = "Weapon_R700.Single"
SWEP.Load_1_Additional_Gun_Cartidge = false
SWEP.ReloadSpeedMultiplier = 1.15
SWEP.MaxShake = 1
SWEP.ForceCalcViewMultiplier = 24
SWEP.AttackSpeedMultiplier = 1
SWEP.AutoReload = false
SWEP.SpreadEyeAnglesMultiplier = .085
SWEP.ShouldPlayFireAnimInZoom = true
SWEP.AttachmentForReloadViewBob = 1
SWEP.ReloadViewBobMultiplier = 0.5
SWEP.EnableViewBobOnDeploy = true
SWEP.FireTypeInZoom = 0
SWEP.DeployPlaybackRate = 0.9
SWEP.CustomTimeFirstDraw = 1.1
SWEP.DeployTime = 1.1
SWEP.DeployTimeMultiplier = 1.65
SWEP.NotEmptyMagazineCycleReload = 0.45
SWEP.MagazineInsertedCycle = 0.6
SWEP.DEBUG_PrintMaterials = false
SWEP.UnitsWhereWillMinimalDamage = 10000
SWEP.UseScope = true
SWEP.DebugCrosshair = false
SWEP.EnableViewBobOnReloading = true

SWEP.WorldModelScale = 0.85	
SWEP.WorldModelOrigin = Vector(-1, 1.6, -1)
SWEP.WorldModelRotate = Angle(9, -1, 0)

SWEP.RunIronSightsPos = Vector(3, 2, -2.2)
SWEP.RunIronSightsAng = Angle(-11, 38.931, 0)

SWEP.AlternativePos = Vector(2, 0, -1)
SWEP.AlternativeAng = Angle(0, 0, 0)

SWEP.ZoomIronSightsPos = Vector(-2.161, 0, 0.319)
SWEP.ZoomIronSightsAng = Angle(0, 0, 0)
