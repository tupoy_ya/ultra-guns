-- https://gamebanana.com/mods/207101

SWEP.Base = "ug_base"
SWEP.Category = "Ultra Guns"
SWEP.PrintName = "DESERT Egle"
SWEP.Author = ""
SWEP.Contact = ""
SWEP.Purpose = ""
SWEP.Instructions = ""
SWEP.Slot = 1

SWEP.ViewModelFOV	= 70
SWEP.ViewModelFlip	= false
SWEP.ViewModel		= "models/weapons/v_pist_deagle.mdl"
SWEP.WorldModel		= "models/weapons/w_pist_deagle.mdl"

SWEP.Spawnable		= true
SWEP.AdminOnly		= false

SWEP.Primary.ClipSize		= 7
SWEP.Primary.DefaultClip	= 9999
SWEP.Primary.TakeBullets		= 1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= ".50 Magnum"
SWEP.Primary.Delay  		= 60 / 247
SWEP.Primary.Spread = 5
SWEP.Primary.Damage = 54
SWEP.Primary.Recoil = .215

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.CSMuzzleX = false
SWEP.CSMuzzleFlashes = false

SWEP.WeaponType = "pistol"
SWEP.HoldType = "pistol"

SWEP.Ignore__sv_defaultdeployspeed = true
SWEP.HasIdleAnimation = false
SWEP.ShakeMultiplier = 16
SWEP.MouseSensivityInZoom = 0.8
SWEP.FireSound = "DE.Fire"
SWEP.Load_1_Additional_Gun_Cartidge = true
SWEP.ReloadSpeedMultiplier = 1.75
SWEP.ForceCalcViewMultiplier = 2.5
SWEP.AttackSpeedMultiplier = .85
SWEP.AutoReload = false
SWEP.SpreadEyeAnglesMultiplier = .085
SWEP.ShouldPlayFireAnimInZoom = false
SWEP.MagazineInsertedCycle = 0.6
SWEP.EnableViewBobOnDeploy = true
SWEP.EnableViewBobOnReloading = true
SWEP.DisableRunningAnimation = true

SWEP.WorldModelOrigin = Vector(-0.5, -0.3, -0.5)

SWEP.RunIronSightsPos = Vector(0.5, -0.15, 0)
SWEP.RunIronSightsAng = Angle(-11, 29.931, 0)

SWEP.AlternativePos = Vector(0, -0.2, 0.84)
SWEP.AlternativeAng = Angle(0, 0, 0)

SWEP.ZoomIronSightsPos = Vector(5.12, 0, 2.79)
SWEP.ZoomIronSightsAng = Angle(0, 0, 0)

SWEP.Animations = {
	["reload"] = "reload",
	["reload_empty"] = "reload",
	["shoot_empty"] = "shoot_empty",
	["first_draw"] = "draw",
	["draw"] = "draw"
}
